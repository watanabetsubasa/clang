#include <stdio.h>
/*
	円の半径を入力し円の面積を表示する
	プログラムを作成する。
*/


int main(void)
{
	int hankei;
	double menseki;
	
	printf("*** 円の面積 ***\n");
	
	printf("円の半径==>");
	scanf("%d",&hankei);//&変数名と書く
	menseki=hankei * hankei * 3.14;
	printf("円の面積は%fです。\n", menseki);
	
	//10×10×3.14=314.000000と表示する
	
	printf("%d×%d=%f\n", hankei,hankei,menseki);
	

	return 0;
}
