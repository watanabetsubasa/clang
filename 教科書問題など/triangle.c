#include <stdio.h>

int main(void)
{
	//三角形の底辺と高さ(いずれも整数)を入力し、面積を表示するプログラム作成する。
	
	int teihen, takasa;
	double menseki;
	
	printf("*** 三角形の面積 ***\n");
	printf("三角形の底辺==>");
	scanf("%d",&teihen);
	printf("三角形の高さ==>");
	scanf("%d",&takasa);
	menseki=teihen*takasa/2.0;
	printf("三角形の面積は%fです。\n",menseki);
	printf("%d×%d÷2=%f\n",teihen,takasa,menseki);
	return 0;
	
	/*
	double teihen,takasa,menseki;
	
	printf("*** 三角形の面積 ***\n");
	printf("三角形の底辺==>");
	scanf("%lf",&teihen);
	printf("三角形の高さ==>");
	scanf("%lf",&takasa);
	menseki=teihen*takasa/2;
	printf("三角形の面積は%fです。",menseki);
	return 0;
	*/
}
