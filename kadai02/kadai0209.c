#include <stdio.h>

int main(void)
{
	int day, day2;
	printf("C言語実習課題2-9 参加賞\n");
	printf("参加日数 ==> ");
	scanf("%d",&day);
	if(1 <= day && day <= 40){
		day2 = (day-1)/10;
		switch(day2){
			case 0:
				printf("参加賞は、鉛筆です。\n");
				break;
			case 1:
				printf("参加賞は、消しゴム、鉛筆です。\n");
				break;
			case 2:
				printf("参加賞は、ノート、消しゴム、鉛筆です。\n");
				break;
			case 3:
				printf("参加賞は、クレヨン、ノート、消しゴム、鉛筆です。\n");
				break;
		}
	}else{
		printf("参加日数は、1から40を入力してください。\n");
	}
	return 0;
}

/*
	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 10
	参加賞は、鉛筆です。

	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 11
	参加賞は、消しゴム、鉛筆です。

	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 30
	参加賞は、ノート、消しゴム、鉛筆です。

	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 31
	参加賞は、クレヨン、ノート、消しゴム、鉛筆です。

	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 0
	参加日数は、1から40を入力してください。

	Z:\clang\kadai02>kadai0209
	C言語実習課題2-9 参加賞
	参加日数 ==> 41
	参加日数は、1から40を入力してください。

	*/