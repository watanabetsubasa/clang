#include <stdio.h>

int main(void)
{
	int seisu1, seisu2, sho, amari;
	printf( "C言語実習課題2-4 除算剰余算\n" );
	printf("被除数 ==> ");
	scanf("%d", &seisu1);
	printf("除数   ==> ");
	scanf("%d", &seisu2);
	if(seisu2 != 0)
	{
		sho = seisu1 / seisu2;
		amari = seisu1 % seisu2;
		if(amari == 0)
		{
			printf("%d / %d = %d\n", seisu1, seisu2, sho);
		}
		else
		{
			printf("%d / %d = %d ... %d\n", seisu1, seisu2, sho, amari);
		}
	}
	else
	{
		printf("0で割ることはできません。\n");
	}
	return 0;
}

/*
	○結果
	Z:\clang\kadai02>kadai0204
	C言語実習課題2-4 除算剰余算
	被除数 ==> 7
	除数   ==> 2
	7 / 2 = 3 ... 1

	Z:\clang\kadai02>kadai0204
	C言語実習課題2-4 除算剰余算
	被除数 ==> 6
	除数   ==> 2
	6 / 2 = 3

	Z:\clang\kadai02>kadai0204
	C言語実習課題2-4 除算剰余算
	被除数 ==> 5
	除数   ==> 0
	0で割ることはできません。

	Z:\clang\kadai02>kadai0204
	C言語実習課題2-4 除算剰余算
	被除数 ==> 0
	除数   ==> 3
	0 / 3 = 0

*/