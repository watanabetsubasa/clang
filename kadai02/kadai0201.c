#include <stdio.h>

int main(void)
{
	int seisu;
	printf("C言語実習課題 2-1 奇数偶数判定\n");
	printf("整数 ==> ");
	scanf("%d", &seisu);
	if(seisu % 2 == 1){
		printf("%dは、奇数です。\n", seisu);
	}else{
		printf("%dは、偶数です。\n", seisu);
	}
	return 0;
}

/*

	○結果
	
	Z:\clang\kadai02>kadai0201
	C言語実習課題 2-1 奇数偶数判定
	整数 ==> 5
	5は、奇数です。

	Z:\clang\kadai02>kadai0201
	C言語実習課題 2-1 奇数偶数判定
	整数 ==> 1
	1は、奇数です。

	Z:\clang\kadai02>kadai0201
	C言語実習課題 2-1 奇数偶数判定
	整数 ==> 123
	123は、奇数です。

	Z:\clang\kadai02>kadai0201
	C言語実習課題 2-1 奇数偶数判定
	整数 ==> 6
	6は、偶数です。

	Z:\clang\kadai02>kadai0201
	C言語実習課題 2-1 奇数偶数判定
	整数 ==> 10000
	10000は、偶数です。

*/