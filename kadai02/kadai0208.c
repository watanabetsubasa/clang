#include <stdio.h>

int main(void)
{
	char moji,komoji,omoji;
	printf("C言語実習課題2-8 文字種判別\n");
	printf("半角１文字 ==> ");
	scanf("%c",&moji);
	if(65 <= moji  && moji <= 90){
		komoji = moji + 32;
		printf("%c は、アルファベット大文字です。\n", moji);
		printf("%c の小文字は、%c です。\n", moji,komoji);
	}else if(97 <= moji && moji <= 122){
		omoji = moji - 32;
		printf("%c は、アルファベット小文字です。\n", moji);
		printf("%c の大文字は、%c です。\n", moji,omoji);
	}else if(48 <= moji && moji <= 57){
		printf("%c は、数字です。\n", moji);
	}else{
		printf("%c は、記号です。\n", moji);
	}
	return 0;
}

/*
	Z:\clang\kadai02>kadai0208
	C言語実習課題2-8 文字種判別
	半角１文字 ==> A
	A は、アルファベット大文字です。
	A の小文字は、a です。

	Z:\clang\kadai02>kadai0208
	C言語実習課題2-8 文字種判別
	半角１文字 ==> g
	g は、アルファベット小文字です。
	g の大文字は、G です。
	
	Z:\clang\kadai02>kadai0208
	C言語実習課題2-8 文字種判別
	半角１文字 ==> 5
	5 は、数字です。

	Z:\clang\kadai02>kadai0208
	C言語実習課題2-8 文字種判別
	半角１文字 ==> !
	! は、記号です。

	Z:\clang\kadai02>kadai0208
	C言語実習課題2-8 文字種判別
	半角１文字 ==> ~
	~ は、記号です。

*/