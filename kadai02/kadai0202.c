#include <stdio.h>

int main(void)
{
	int tensu;
	printf("C言語実習課題2-2 点数評価\n");
	printf("点数 ==> ");
	scanf("%d", &tensu);
	if(0 <= tensu && tensu <= 100)
	{
		if(80 <= tensu)
		{
			printf("%d点、Aです。\n", tensu);
		}
		else if(70 <= tensu)
		{
			printf("%d点、Bです。\n", tensu);
		}
		else if(60 <= tensu)
		{
			printf("%d点、Cです。\n", tensu);
		}
		else
		{
			printf("%d点、Dです。\n", tensu);
		}
	}
	else
	{
		printf("0から100を入力してください。\n");
	}

	return 0;
}

/*
	○結果
	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 100
	100点、Aです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 90
	90点、Aです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 80
	80点、Aです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 79
	79点、Bです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 75
	75点、Bです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 70
	70点、Bです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 69
	69点、Cです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 65
	65点、Cです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 60
	60点、Cです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 59
	59点、Dです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 30
	30点、Dです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 0
	0点、Dです。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> -1
	0から100を入力してください。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> -50
	0から100を入力してください。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 101
	0から100を入力してください。

	Z:\clang\kadai02>kadai0202
	C言語実習課題2-2 点数評価
	点数 ==> 230
	0から100を入力してください。

*/