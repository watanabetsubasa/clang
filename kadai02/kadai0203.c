#include <stdio.h>

int main(void)
{
	int month;
	printf( "C言語実習課題2-3 季節判定\n" );
	printf("月 ==> ");
	scanf("%d", &month);
	switch(month)
	{
		case 3: case 4: case 5:
			printf("%d月は、春です。\n", month);
			break;
		case 6: case 7: case 8:
			printf("%d月は、夏です。\n", month);
			break;
		case 9: case 10: case 11:
			printf("%d月は、秋です。\n", month);
			break;
		case 12: case 1: case 2:
			printf("%d月は、冬です。\n", month);
			break;
		default:
			printf("1から12を入力してください。\n", month);
	}
	return 0;
}

/*
	○結果
	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 3
	3月は、春です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 4
	4月は、春です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 5
	5月は、春です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 6
	6月は、夏です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 7
	7月は、夏です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 8
	8月は、夏です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 9
	9月は、秋です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 10
	10月は、秋です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 11
	11月は、秋です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 12
	12月は、冬です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 1
	1月は、冬です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 2
	2月は、冬です。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 0
	1から12を入力してください。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> -50
	1から12を入力してください。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 13
	1から12を入力してください。

	Z:\clang\kadai02>kadai0203
	C言語実習課題2-3 季節判定
	月 ==> 230
	1から12を入力してください。

	*/