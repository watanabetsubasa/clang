#include <stdio.h>

int main(void)
{
	int year;
	printf("C言語実習課題 2-6 うるう年判定１\n");
	printf("西暦４桁 ==> ");
	scanf("%d", &year);
	if(year % 400 == 0)
	{
		printf("%d年は、うるう年です。\n", year);
	}
	else if(year % 4 == 0)
	{
			if(year % 100 != 0)
			{
				printf("%d年は、うるう年です。\n", year);
			}
			else
			{
				printf("%d年は、うるう年ではありません。\n", year);
			}
	}
	else
	{
		printf("%d年は、うるう年ではありません。\n", year);
	}
	return 0;
}

/*
	
	Z:\clang\kadai02>kadai0206
	C言語実習課題 2-6 うるう年判定１
	西暦４桁 ==> 2020
	2020年は、うるう年です。

	Z:\clang\kadai02>kadai0206
	C言語実習課題 2-6 うるう年判定１
	西暦４桁 ==> 2021
	2021年は、うるう年ではありません。

	Z:\clang\kadai02>kadai0206
	C言語実習課題 2-6 うるう年判定１
	西暦４桁 ==> 2100
	2100年は、うるう年ではありません。

	Z:\clang\kadai02>kadai0206
	C言語実習課題 2-6 うるう年判定１
	西暦４桁 ==> 2400
	2400年は、うるう年です。

*/