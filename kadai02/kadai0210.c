#include <stdio.h>

int main(void)
{
	char kubun;
	int seisu1,seisu2,answer;
	printf("C言語実習課題 2-10\n");
	printf("計算区分(s:符号付き u:符号なし) ==> ");
	scanf("%c",&kubun);
	if(kubun=='s'){
		printf("符号付き整数1 ==> ");
		scanf("%d",&seisu1);
		printf("符号付き整数2 ==> ");
		scanf("%d",&seisu2);
		answer=seisu1+seisu2;
		printf("答えは、%dです。\n",answer);
	}else if(kubun=='u'){
		printf("符号なし整数1 ==> ");
		scanf("%d",&seisu1);
		printf("符号なし整数2 ==> ");
		scanf("%d",&seisu2);
		answer=seisu1+seisu2;
		printf("答えは、%dです。\n",answer);
	}else{
		printf("計算区分エラー\n");
		}
	return 0;
}
