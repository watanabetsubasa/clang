#include <stdio.h>

int main(void)
{
	int gozen, gogo;
	printf("C言語実習課題 2-5 合否判定\n");
	printf("午前試験点数 ==> ");
	scanf("%d", &gozen);
	if(gozen >= 60)
	{
		printf("午後試験点数 ==> ");
		scanf("%d", &gogo);
		if(gogo >= 60)
		{
			printf("合格です。\n");
		}
		else
		{
			printf("不合格です。\n");
		}
	}
	else
	{
		printf("不合格です。\n");
	}
	return 0;
}
/*
	Z:\clang\kadai02>kadai0205
	C言語実習課題 2-5 合否判定
	午前試験点数 ==> 60
	午後試験点数 ==> 60
	合格です。

	Z:\clang\kadai02>kadai0205
	C言語実習課題 2-5 合否判定
	午前試験点数 ==> 100
	午後試験点数 ==> 59
	不合格です。

	Z:\clang\kadai02>kadai0205
	C言語実習課題 2-5 合否判定
	午前試験点数 ==> 59
	不合格です。

*/