#include <stdio.h>

int main(void)
{
	int head=0,tail=-1,choice,queue[6];
	
	printf("C言語実習課題11-8 キュー\n*** キュー数 5 ***\n");
	
	printf("処理選択 (0:enqueue 1:dequeue 9:end) ==> ");
	scanf("%d",&choice);
	
	while(choice!=9){
		switch(choice){
			//エンキュー
			case 0:
				if((tail+2)%6!=head){
					tail++;
					printf("　　整数 ==> ");
					scanf("%d",&queue[tail%6]);
				}else{
					printf("　　キューが一杯です\n");
				}
				break;
			//デキュー
			case 1:
				if((tail+1)%6!=head){
					printf("　　データ： %d\n",queue[head%6]);
					head++;
				}else{
					printf("　　キューが空です\n");
				}
				break;
		}
		printf("処理選択 (0:enqueue 1:dequeue 9:end) ==> ");
		scanf("%d",&choice);
		
	}
	
	return 0;
}
/*
Z:\clang\kadai11>kadai1108
C言語実習課題11-8 キュー
*** キュー数 5 ***
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 50
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 20
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 30
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 50
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 20
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 40
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 30
処理選択 (0:enqueue 1:dequeue 9:end) ==> 9

Z:\clang\kadai11>kadai1108
C言語実習課題11-8 キュー
*** キュー数 5 ***
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　キューが空です
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 10
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 20
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 10
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 20
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　キューが空です
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 30
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 40
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 50
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 60
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　整数 ==> 70
処理選択 (0:enqueue 1:dequeue 9:end) ==> 0
　　キューが一杯です
処理選択 (0:enqueue 1:dequeue 9:end) ==> 1
　　データ： 30
処理選択 (0:enqueue 1:dequeue 9:end) ==> 2
処理選択 (0:enqueue 1:dequeue 9:end) ==> 9
*/