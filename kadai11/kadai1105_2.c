#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_RECORD_LEN 80

int main() {
    char record[MAX_RECORD_LEN];
    int line_number = 0;

    while (fgets(record, sizeof(record), stdin) != NULL) {
        // 改行文字を削除
        record[strcspn(record, "\n")] = '\0';

        // フラグを0に初期化
        int sp_flag = 0; // 行に空白があるならエラー
        int cm_flag = 0; // 行にカンマが3つでないならエラー
        int cl_flag = 0; // クラスがA, B, C以外ならエラー
        int no_flag = 0; // NOが1から5以外ならエラー
        int na_flag = 0; // 氏名が規定のフォーマットに合わないならエラー
        int po_flag = 0; // 得点が0から100の範囲外ならエラー

        line_number++; // 行番号を更新

        // スペースの有無を確認
        if (strchr(record, ' ') != NULL) {
            sp_flag = 1;
        }

        // レコードのカンマの数を確認
        int commaCount = 0;
        for (int i = 0; record[i] != '\0'; i++) {
            if (record[i] == ',') {
                commaCount++;
            }
        }

        // カンマが3つ含まれているか確認
        if (commaCount != 3) {
            cm_flag = 1;
        } else {
            // カンマが3つ含まれている場合、クラス、NO、氏名、得点のチェックを行う
            char classChar = record[0];
            int noInt = record[2] - '0';
            char name[MAX_RECORD_LEN];
            int nameCount = 0;
            int scoreInt = 0;

            // クラスのチェック
            if (classChar != 'A' && classChar != 'B' && classChar != 'C') {
                cl_flag = 1;
            }

            // NOのチェック
            if (noInt < 1 || noInt > 5) {
                no_flag = 1;
            }

            // 氏名のチェック
            int i;
            for (i = 4; record[i] != ',' && record[i] != '\0'; i++) {
                if (nameCount == 0) {
                    if (!isupper(record[i])) {
                        na_flag = 1;
                    }
                } else {
                    if (!islower(record[i])) {
                        na_flag = 1;
                    }
                }
                name[nameCount++] = record[i];
            }
            name[nameCount] = '\0';

            // 名前が1〜10文字でない場合、または先頭が大文字でない場合
            if (nameCount < 1 || nameCount > 10 || !isupper(name[0])) {
                na_flag = 1;
            }

            // 得点のチェック
            i++; // カンマの次の位置
            while (record[i] != '\0') {
                if (!isdigit(record[i])) {
                    po_flag = 1;
                    break;
                }
                scoreInt = scoreInt * 10 + (record[i] - '0');
                i++;
            }

            // 得点が0〜100の範囲内かチェック
            if (scoreInt < 0 || scoreInt > 100) {
                po_flag = 1;
            }
        }

        // エラーがある場合、エラーデータを出力
        if (sp_flag || cm_flag || cl_flag || no_flag || na_flag || po_flag) {
            fprintf(stdout, "%d:", line_number);
            fprintf(stdout, "%c,%d,%s,%d:", record[0], record[2] - '0', name, scoreInt);
            if (sp_flag) {
                fprintf(stdout, "SP,");
            }
            if (cm_flag) {
                fprintf(stdout, "CM,");
            }
            if (cl_flag) {
                fprintf(stdout, "CL,");
            }
            if (no_flag) {
                fprintf(stdout, "NO,");
            }
            if (na_flag) {
                fprintf(stdout, "NA,");
            }
            if (po_flag) {
                fprintf(stdout, "PO,");
            }
            fprintf(stdout, "\n");
        }
    }

    return 0;
}
