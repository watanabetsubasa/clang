#include <stdio.h>
#include <stdlib.h>
int compare(const void *a,const void *b);

int main(void)
{
	int n,i,*p,*pi;
	
	printf("C言語実習課題11-4 calloc と qsort\n");
	
	printf("データ個数 ==> ");
	scanf("%d",&n);
	
	p=calloc(n,sizeof(int));
	if(p==NULL){
		printf("メモリ確保ができません。\n");
	}else{
		for(i=0;i<n;i++){
			printf("整数(%d/%d) ==> ",i+1,n);
			scanf("%d",p+i);
		}
		
		printf("*** ソート前 ***\n");
		for(pi=p;pi<p+n;pi++){
			printf("%4d",*pi);
		}
		
		qsort(p,n,sizeof(int),compare);
		
		printf("\n*** ソート後 ***\n");
		for(pi=p;pi<p+n;pi++){
			printf("%4d",*pi);
		}
		printf("\n");
		
		free(p);
	}
	return 0;
}

int compare(const void *a,const void *b)
{
	return *(int *)a-*(int *)b;
}
/*
Z:\clang\kadai11>kadai1104
C言語実習課題11-4 calloc と qsort
データ個数 ==> 7
整数(1/7) ==> 20
整数(2/7) ==> 60
整数(3/7) ==> 10
整数(4/7) ==> 50
整数(5/7) ==> 70
整数(6/7) ==> 40
整数(7/7) ==> 30
*** ソート前 ***
  20  60  10  50  70  40  30
*** ソート後 ***
  10  20  30  40  50  60  70

Z:\clang\kadai11>kadai1104
C言語実習課題11-4 calloc と qsort
データ個数 ==> 1
整数(1/1) ==> 10
*** ソート前 ***
  10
*** ソート後 ***
  10
*/