#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_NAME_LENGTH 100

// 番号と氏名を格納するための構造体
typedef struct Node {
    int number;
    char name[MAX_NAME_LENGTH];
    struct Node* next;
} Node;

Node* create_node(int number, const char* name);
void add_node(int number, const char* name);
void remove_node(int number);
void display_list(void);
void free_list(void);

// リストの先頭ポインタ
Node* head = NULL;

int main(void) {
	int choice, number;
	char name[MAX_NAME_LENGTH];
	
	printf("C言語実習課題11-9 リスト\n");

	do {
		// 処理選択をユーザーに表示
		printf("処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> ");
		scanf("%d", &choice);

        switch (choice) {
            case 0:
                // 追加の場合
                printf("　追加する番号 ==> ");
                scanf("%d", &number);

                // 既に同じ番号がリストに存在するかチェック
                Node* current = head;
                while (current != NULL) {
                    if (current->number == number) {
                        printf("　　同じ番号があります。追加しません。\n");
                        break;
                    }
                    current = current->next;
                }

                if (current == NULL) {
                    printf("　追加する氏名 ==> ");
                    scanf("%s", name);
                    add_node(number, name);
                }
                break;
            case 1:
                // 削除の場合
                printf("　削除する番号 ==> ");
                scanf("%d", &number);
                remove_node(number);
                break;
            case 2:
                // 一覧の場合
                display_list();
                break;
        }

    } while (choice != 9);

    // プログラム終了時にメモリを解放
    free_list();

    return 0;
}

// 新しい要素を作成する関数
Node* create_node(int number, const char* name) {
    Node* new_node = (Node*)malloc(sizeof(Node));
    if (new_node != NULL) {
        new_node->number = number;
        strncpy(new_node->name, name, MAX_NAME_LENGTH - 1);
        new_node->name[MAX_NAME_LENGTH - 1] = '\0'; // Ensure null-terminated
        new_node->next = NULL;
    }
    return new_node;
}

// リストに要素を追加する関数
void add_node(int number, const char* name) {
    Node* current = head;
    Node* prev = NULL;

    while (current != NULL) {
        if (current->number > number) {
            // 番号の昇順になるようにリストに追加
            Node* new_node = create_node(number, name);
            if (new_node == NULL) {
                printf("メモリ確保エラー\n");
                return;
            }

            if (prev == NULL) {
                // 先頭に追加する場合
                new_node->next = head;
                head = new_node;
            } else {
                // 中間に追加する場合
                prev->next = new_node;
                new_node->next = current;
            }

            return;
        }

        prev = current;
        current = current->next;
    }

    // リストが空の場合または末尾に追加する場合
    Node* new_node = create_node(number, name);
    if (new_node == NULL) {
        printf("メモリ確保エラー\n");
        return;
    }

    if (prev == NULL) {
        // リストが空の場合
        head = new_node;
    } else {
        // 末尾に追加する場合
        prev->next = new_node;
    }
}

// リストから要素を削除する関数
void remove_node(int number) {
    Node* current = head;
    Node* prev = NULL;
    int rm_flag=0;

    while (current != NULL) {
        if (current->number == number) {
            // 要素が見つかった場合
            if (prev == NULL) {
                // 見つかった要素がリストの先頭の場合
                head = current->next;
            } else {
                // 見つかった要素がリストの中間か末尾の場合
                prev->next = current->next;
            }
            printf("　　%d %s を削除しました。\n", current->number, current->name);
           rm_flag=1;
            break;
        }

        prev = current;
        current = current->next;
    }

    if (rm_flag==0) {
        printf("　　番号[%d]は、見つかりません。\n", number);
    } else {
        // リスト全体を解放して、再度メモリ領域を確保し直してリストを再構築
        Node* temp_head = head;
        head = NULL;

        while (temp_head != NULL) {
        	if(temp_head != current){
            	add_node(temp_head->number, temp_head->name);
        	}
            Node* next = temp_head->next;
            free(temp_head);
            temp_head = next;
        }
    }
}

// リストの内容を表示する関数
void display_list(void) {
    if (head == NULL) {
        printf("\nリストは空です。\n");
        return;
    }

    printf("\n*** 一覧表 ***\n");
    printf("No 氏名\n");
    printf("-- ----------\n");

    Node* current = head;
    while (current != NULL) {
        printf("%-2d %-10s\n", current->number, current->name);
        current = current->next;
    }
    printf("\n");
}

// リストの要素をすべて解放する関数
void free_list(void) {
    Node* current = head;
    while (current != NULL) {
        Node* next = current->next;
        free(current);
        current = next;
    }
    head = NULL;
}

/*
Z:\clang\kadai11>kadai1109
C言語実習課題11-9 リスト
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 20
　追加する氏名 ==> Jiro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 30
　追加する氏名 ==> Saburo
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 10
　追加する氏名 ==> Taro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 2

*** 一覧表 ***
No 氏名
-- ----------
10 Taro
20 Jiro
30 Saburo

処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 9

Z:\clang\kadai11>kadai1109
C言語実習課題11-9 リスト
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 30
　追加する氏名 ==> Saburo
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 10
　追加する氏名 ==> Taro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 2

*** 一覧表 ***
No 氏名
-- ----------
10 Taro
30 Saburo

処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 20
　追加する氏名 ==> Jiro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 1
　削除する番号 ==> 30
　　30 Saburo を削除しました。
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 2

*** 一覧表 ***
No 氏名
-- ----------
10 Taro
20 Jiro

処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 9

Z:\clang\kadai11>kadai1109
C言語実習課題11-9 リスト
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 30
　追加する氏名 ==> Saburo
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 10
　追加する氏名 ==> Taro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 20
　追加する氏名 ==> Jiro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 1
　削除する番号 ==> 10
　　10 Taro を削除しました。
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 1
　削除する番号 ==> 50
　　番号[50]は、見つかりません。
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 20
　　同じ番号があります。追加しません。
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 1
　削除する番号 ==> 30
　　30 Saburo を削除しました。
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 0
　追加する番号 ==> 50
　追加する氏名 ==> Goro
処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 2

*** 一覧表 ***
No 氏名
-- ----------
20 Jiro
50 Goro

処理選択 (0:追加 1:削除 2:一覧 9:終了) ==> 9
*/
