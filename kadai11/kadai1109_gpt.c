#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 100

// 番号と氏名を格納するための構造体
typedef struct Node {
    int number;
    char name[MAX_NAME_LENGTH];
    struct Node* next;
} Node;

// リストの先頭ポインタ
Node* head = NULL;

// 新しい要素を作成する関数
Node* create_node(int number, const char* name) {
    Node* new_node = (Node*)malloc(sizeof(Node));
    if (new_node != NULL) {
        new_node->number = number;
        strncpy(new_node->name, name, MAX_NAME_LENGTH - 1);
        new_node->name[MAX_NAME_LENGTH - 1] = '\0'; // Ensure null-terminated
        new_node->next = NULL;
    }
    return new_node;
}

// リストに要素を追加する関数
void add_node(int number, const char* name) {
    // 既に同じ番号がリストに存在するかチェック
    Node* current = head;
    while (current != NULL) {
        if (current->number == number) {
            printf("同じ番号があります。追加しません。\n");
            return;
        }
        current = current->next;
    }

    Node* new_node = create_node(number, name);
    if (new_node == NULL) {
        printf("メモリ確保エラー\n");
        return;
    }

    // リストが空の場合
    if (head == NULL) {
        head = new_node;
    } else {
        // リストの末尾まで移動
        current = head;
        while (current->next != NULL) {
            current = current->next;
        }
        // 末尾に新しい要素を追加
        current->next = new_node;
    }

    printf("要素を追加しました: 番号:%d, 氏名:%s\n", number, name);
}

// リストから要素を削除する関数
void remove_node(int number) {
    Node* current = head;
    Node* prev = NULL;

    while (current != NULL) {
        if (current->number == number) {
            // 要素が見つかった場合
            if (prev == NULL) {
                // 見つかった要素がリストの先頭の場合
                head = current->next;
            } else {
                // 見つかった要素がリストの中間か末尾の場合
                prev->next = current->next;
            }
            free(current);
            printf("要素を削除しました: 番号:%d\n", number);
            return;
        }

        prev = current;
        current = current->next;
    }

    printf("要素が見つかりませんでした: 番号:%d\n", number);
}

// リストの内容を表示する関数
void display_list() {
    if (head == NULL) {
        printf("リストは空です。\n");
        return;
    }

    printf("一覧表\n");
    printf("No 氏名\n");
    printf("-- ----------\n");

    Node* current = head;
    while (current != NULL) {
        printf("%-2d %-10s\n", current->number, current->name);
        current = current->next;
    }
}

// リストの要素をすべて解放する関数
void free_list() {
    Node* current = head;
    while (current != NULL) {
        Node* next = current->next;
        free(current);
        current = next;
    }
    head = NULL;
}

int main() {
    int choice, number;
    char name[MAX_NAME_LENGTH];

    do {
        // 処理選択をユーザーに表示
        printf("\n処理を選択してください\n");
        printf("0: 追加\n");
        printf("1: 削除\n");
        printf("2: 一覧\n");
        printf("9: 終了\n");
        printf("選択: ");
        scanf("%d", &choice);

        switch (choice) {
            case 0:
                // 追加の場合
                printf("追加する番号 ==> ");
                scanf("%d", &number);

                // 既に同じ番号がリストに存在するかチェック
                Node* current = head;
                while (current != NULL) {
                    if (current->number == number) {
                        printf("同じ番号があります。追加しません。\n");
                        break;
                    }
                    current = current->next;
                }

                if (current == NULL) {
                    printf("追加する氏名 ==> ");
                    scanf("%s", name);
                    add_node(number, name);
                }
                break;
            case 1:
                // 削除の場合
                printf("削除する番号 ==> ");
                scanf("%d", &number);
                remove_node(number);
                break;
            case 2:
                // 一覧の場合
                display_list();
                break;
            case 9:
                // 終了の場合
                printf("プログラムを終了します。\n");
                break;
            default:
                printf("無効な選択です。再度選択してください。\n");
        }

    } while (choice != 9);

    free_list();

    return 0;
}
