#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void)
{
	char data[100],*token;
	int i,j,point[20],count,sum=0,max,min,rank[20];
	double avg,sqDevSum=0,var,stdDev,z_score[20];
	
	//一行を読み込む
	fgets(data,sizeof(data),stdin);
	//改行文字を削除
	data[strcspn(data,"\n")]='\0';
	//カンマごとに分けてint型配列に格納
	token=strtok(data,",");
	for (i=0;token!=NULL&&i<20;i++){
		point[i]=atoi(token);
		token=strtok(NULL,",");
	}
	//件数を記録
	count=i;
	
	//合計、最大、最小、平均、順位を求める
	for(i=0,max=point[0],min=point[0];i<count;i++){
		sum+=point[i];
		if(max<point[i]){
			max=point[i];
		}
		if(min>point[i]){
			min=point[i];
		}
		
		rank[i]=1;
		for(j=0;j<count;j++){
			if(point[i]<point[j]){
				rank[i]++;
			}
		}
	}
	avg=(double)sum/count;
	
	//分散と標準偏差を求める
	for(i=0;i<count;i++){
		sqDevSum+=pow(point[i]-avg,2.0);
	}
	var=sqDevSum/count;
	stdDev=sqrt(var);
	
	//偏差値を求める
	for(i=0;i<count;i++){
		z_score[i]=(point[i]-avg)*10/stdDev+50;
	}
	
	
	
	//結果を出力
	printf("C言語実習課題 11-6 成績一覧表\n\n *** 成績一覧表 ***\n NO  点 偏差値 順位\n--- --- ------ ----\n");
	for(i=0;i<count;i++){
		printf("%3d %3d %6.2f %4d\n",i+1,point[i],z_score[i],rank[i]);
	}
	printf("データ件数:%d\n　　　合計:%d\n　　　平均:%.2f\n　　　最高:%d\n　　　最低:%d\n　　　分散:%.2f\n　標準偏差:%.2f\n",count,sum,avg,max,min,var,stdDev);
	
	return 0;
}

/*
Z:\clang\kadai11>type seiseki2.txt
62,98,62,83,75,85,77,65,91,79,70,84,73,55,99,81,52,78,68,88

Z:\clang\kadai11>kadai1106<seiseki2.txt>ichiran.txt

Z:\clang\kadai11>type ichiran.txt
C言語実習課題 11-6 成績一覧表

 *** 成績一覧表 ***
 NO  点 偏差値 順位
--- --- ------ ----
  1  62  38.83   17
  2  98  67.05    2
  3  62  38.83   17
  4  83  55.29    7
  5  75  49.02   12
  6  85  56.86    5
  7  77  50.59   11
  8  65  41.18   16
  9  91  61.56    3
 10  79  52.16    9
 11  70  45.10   14
 12  84  56.08    6
 13  73  47.45   13
 14  55  33.34   19
 15  99  67.84    1
 16  81  53.72    8
 17  52  30.99   20
 18  78  51.37   10
 19  68  43.53   15
 20  88  59.21    4
データ件数:20
　　　合計:1525
　　　平均:76.25
　　　最高:99
　　　最低:52
　　　分散:162.69
　標準偏差:12.75

*/