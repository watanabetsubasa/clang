#include <stdio.h>

int main(void)
{
	int moji,byteCnt=0,lineCnt=0;
	
	printf("C言語実習課題11-1 テキストファイルのバイト数と行数\n");
	
	moji=getchar();
	while(moji!=EOF){
		byteCnt++;
		
		if(moji=='\n'){
			byteCnt++;
			lineCnt++;
		}
		moji=getchar();
	}
	
	printf("バイト数：%d\n",byteCnt);
	printf("行　数：%d\n",lineCnt);
	
	return 0;
}
/*
Z:\clang\kadai11>type data1.txt
abcde
1234567890
***
ABCDEFGHIJKLMNOPQRSTUVWXYZ

Z:\clang\kadai11>kadai1101<data1.txt
C言語実習課題11-1 テキストファイルのバイト数と行数
バイト数：52
行　数：4
*/