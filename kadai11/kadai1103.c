#include <stdio.h>
int input(char *p,int maxByte);
void output(char *p);

int main(void)
{
	int byte;
	char str[11];
	
	printf("C言語実習課題11-3 文字数制限付き文字列入力\n\n*** 文字列入出力（最大 10 バイト）***\n");
	
	byte=input(str,10);
	
	if(byte==0){
		printf("入力がありません。\n");
	}else if(byte==EOF){
		printf("10バイトを超えています。\n");
	}else{
		output(str);
	}
	
	return 0;
}

int input(char *p,int maxByte)
{
	int ch;
	int byte=0;
	printf("入力 ==> ");
	ch=getchar();
	while(ch!='\n'&&ch!=EOF&&byte<maxByte){
		*p=ch;
		byte++;
		ch=getchar();
		p++;
	}
	*p='\0';
	
	while(ch!='\n'&&ch!=EOF){
		ch=getchar();
		byte++;
	}
	
	if(byte>maxByte){
		byte=EOF;
	}
	
	return byte;
}

void output(char *p)
{
	printf("出力  :  ");
	while(*p!='\0'){
		putchar(*p);
		p++;
	}
	putchar('\n');
}

/*
Z:\clang\kadai11>kadai1103
C言語実習課題11-3 文字数制限付き文字列入力

*** 文字列入出力（最大 10 バイト）***
入力 ==> abcde
出力  :  abcde

Z:\clang\kadai11>kadai1103
C言語実習課題11-3 文字数制限付き文字列入力

*** 文字列入出力（最大 10 バイト）***
入力 ==> abcdefghij
出力  :  abcdefghij

Z:\clang\kadai11>kadai1103
C言語実習課題11-3 文字数制限付き文字列入力

*** 文字列入出力（最大 10 バイト）***
入力 ==> abcdefghijk
10バイトを超えています。

Z:\clang\kadai11>kadai1103
C言語実習課題11-3 文字数制限付き文字列入力

*** 文字列入出力（最大 10 バイト）***
入力 ==>
入力がありません。

*/