#include <stdio.h>

int main(void)
{
	int choice,stack[5],stock=0;
	
	printf("C言語実習課題11-7 スタック\n*** スタック数 5 ***\n");
	
	printf("処理選択 (0:push 1:pop 9:end) ==> ");
	scanf("%d",&choice);
	
	//9をはじく
	while(choice!=9){
		//0,1以外をはじく
		if(choice==0||choice==1){
			//push処理
			if(choice==0){
				if(stock<5){
					printf("　　整数 ==> ");
					scanf("%d",&stack[stock]);
					stock++;
				}else{
					printf("　　スタックが一杯です\n");
				}
			}else{ //pop処理
				if(stock>0){
					printf("　　データ： %d\n",stack[stock-1]);
					stock--;
				}else{
					printf("　　スタックが空です\n");
				}
			}
		}
		
		printf("処理選択 (0:push 1:pop 9:end) ==> ");
		scanf("%d",&choice);
	}
	
	return 0;
}
/*
Z:\clang\kadai11>kadai1107
C言語実習課題11-7 スタック
*** スタック数 5 ***
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 50
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 20
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 30
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 30
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 20
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 40
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 40
処理選択 (0:push 1:pop 9:end) ==> 9

Z:\clang\kadai11>kadai1107
C言語実習課題11-7 スタック
*** スタック数 5 ***
処理選択 (0:push 1:pop 9:end) ==> 1
　　スタックが空です
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 10
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 20
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 20
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 10
処理選択 (0:push 1:pop 9:end) ==> 1
　　スタックが空です
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 30
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 40
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 50
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 60
処理選択 (0:push 1:pop 9:end) ==> 0
　　整数 ==> 70
処理選択 (0:push 1:pop 9:end) ==> 0
　　スタックが一杯です
処理選択 (0:push 1:pop 9:end) ==> 1
　　データ： 70
処理選択 (0:push 1:pop 9:end) ==> 2
処理選択 (0:push 1:pop 9:end) ==> 9

*/