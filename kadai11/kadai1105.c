#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int main(void)
{
	char output[100],record[82],cls[81],no[81],name[81],point[81],*newline; 
	int recCnt=0,noInt,pointInt,i,sp_flag,cm_flag,cl_flag,no_flag,na_flag,po_flag,commaCount;
	
	while(fgets(record,sizeof(record),stdin)!=NULL){
		//フラグを0に設定
		sp_flag=0;//空白エラーフラグ
		cm_flag=0;//カンマエラーフラグ
		cl_flag=0;//クラスエラーフラグ
		no_flag=0;//NOエラーフラグ
		na_flag=0;//氏名エラーフラグ
		po_flag=0;//得点エラー
		//レコード数をカウント
		recCnt++;
		
		//改行文字を削除
		newline=strchr(record,'\n'); 
		if (newline != NULL) {
			*newline = '\0';
		}
		
		//�@スペースの有無を確認
		if(strchr(record,' ')!=NULL){
			sp_flag=1;
		}
		
		//�Aレコードのカンマの数を確認
		commaCount=0;
		for (i = 0; record[i] != '\0'; i++) {
			if (record[i] == ',') {
				commaCount++;
			}
		}
		if(commaCount!=3){
			cm_flag=1;
		}
			
		if(sp_flag==0&&cm_flag==0){
			
			//文字列から各要素を取り出す
			sscanf(record,"%[^,],%[^,],%[^,],%[^,]",cls,no,name,point);
			
			//�Bクラスの確認
			if(strcmp(cls, "A")!=0&&strcmp(cls, "B")!=0&&strcmp(cls, "C")!=0){
				cl_flag=1;
			}
			
			//�CNOが1〜5か確認
			if(strlen(no)==1){
				noInt=atoi(no);
				if(noInt<1||5<noInt){
					no_flag=1;
				}
			}else{
				no_flag=1;
			}
			
			//�D氏名判定
			if(1<=strlen(name)&&strlen(name)<=10){
				if(isupper(name[0])){
					for(i=1;name[i]!='\0';i++){
						if(!islower(name[i])){
							na_flag=1;
						}
					}
				}else{
					na_flag=1;
				}
			}else{
				na_flag=1;
			}
			
			//�E得点判定
			if(1<=strlen(point)&&strlen(point)<=3){
				for(i=0;point[i]!='\0';i++){
					if(!isdigit(point[i])){
						po_flag=1;
					}
				}
				pointInt=atoi(point);
				if(pointInt<0||100<pointInt){
					po_flag=1;
				}
			}else{
				po_flag=1;
			}
		}
		//出力
		if(sp_flag==1||cm_flag==1||cl_flag==1||no_flag==1||na_flag==1||po_flag==1){
			snprintf(output, sizeof(output),"%d",recCnt);
			strcat(output,":");
			strcat(output,record);
			strcat(output,":");
			if(sp_flag==1){
				strcat(output,"SP,");
			}
			if(cm_flag==1){
				strcat(output,"CM,");
			}
			if(cl_flag==1){
				strcat(output,"CL,");
			}
			if(no_flag==1){
				strcat(output,"NO,");
			}
			if(na_flag==1){
				strcat(output,"NA,");
			}
			if(po_flag==1){
				strcat(output,"PO,");
			}
			output[strlen(output)-1] ='\0';
			fputs(output,stdout);
			fputs("\n",stdout);
		}
	}
	return 0;
}
/*
Z:\clang\kadai11>type error.dat
3:A,3,Naka mura,79:SP
4:a,4,Kobayashi,930:CL,PO
5:A 5 Kato 100:SP,CM
8:E,8,SASAKI,1000:CL,NO,NA,PO
9:B,4,YamaguchiTaro,81:NA
10:B,5 Matsumoto,75:SP,CM
12:C,2,INOUE,99:NA
13:C,3.Kimura.80:CM
14:C,4,hayshi,ZI:NA,PO
*/