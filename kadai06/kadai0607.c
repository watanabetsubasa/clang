#include <stdio.h>
int kaijo(int seisu);


int main(void)
{
	int seisu,result;
	printf("C言語実習課題6-7 階乗（再帰）\n");
	printf("整数 ==> ");
	scanf("%d",&seisu);
	result=kaijo(seisu);
	printf("%dの階乗は、%dです。\n",seisu,result);
	return 0;
}

int kaijo(int seisu)
{
	int result;
	if(seisu>0){
		result=kaijo(seisu-1);
		result=seisu*result;
	}else{
		result=1;
	}
	return result;
}

/*
Z:\clang\kadai06>kadai0607
C言語実習課題6-7 階乗（再帰）
整数 ==> 5
5の階乗は、120です。

Z:\clang\kadai06>kadai0607
C言語実習課題6-7 階乗（再帰）
整数 ==> 1
1の階乗は、1です。

Z:\clang\kadai06>kadai0607
C言語実習課題6-7 階乗（再帰）
整数 ==> 0
0の階乗は、1です。

Z:\clang\kadai06>kadai0607
C言語実習課題6-7 階乗（再帰）
整数 ==> 12
12の階乗は、479001600です。
*/