#include <stdio.h>

int multipiler(int num);

int main(void)
{
	int num,result;
	printf("C言語実習課題 6-1 二乗計算\n");
	printf("整数 ==> ");
	scanf("%d",&num);
	result=multipiler(num);
	printf("%dの二乗は、%dです。\n",num,result);
	return 0;
}

int multipiler(int num)
{
	int result;
	result=num*num;
	return result;
}
/*
Z:\clang\kadai06>kadai0601
C言語実習課題 6-1 二乗計算
整数 ==> 5
5の二乗は、25です。

Z:\clang\kadai06>kadai0601
C言語実習課題 6-1 二乗計算
整数 ==> 100
100の二乗は、10000です。

Z:\clang\kadai06>kadai0601
C言語実習課題 6-1 二乗計算
整数 ==> 1
1の二乗は、1です。

Z:\clang\kadai06>kadai0601
C言語実習課題 6-1 二乗計算
整数 ==> 0
0の二乗は、0です。

*/