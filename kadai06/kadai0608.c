#include <stdio.h>
int input(void);
int search(int in_code);
void output(int in_code,int zaiko);

int main(void)
{
	int in_code,zaiko;
	printf("C言語実習課題6-8 在庫数検索\n");
	in_code=input();
	while(in_code!=99){
		zaiko=search(in_code);
		output(in_code,zaiko);
		in_code=input();
	}
	return 0;
}

int input(void)
{
	int in_code;
	do{
		printf("商品コード ==> ");
		scanf("%d",&in_code);
		if(!(10<=in_code&&in_code<=90||in_code==99)){
			printf("商品コードは、10から90です。\n");
		}
	}while(!(10<=in_code&&in_code<=90||in_code==99));
	return in_code;
}

int search(int in_code)
{
	int hit_flag=0,i,code[5]={21,68,37,40,85},stock[5]={123,430,333,650,200},zaiko;
	for(i=0;i<5&&hit_flag==0;i++){
		if(in_code==code[i]){
			hit_flag=1;
		}
	}
	if(hit_flag==1){
		zaiko=stock[i-1];
	}else{
		zaiko=-1;
	}
	return zaiko;
}

void output(int in_code,int zaiko)
{
	if(zaiko!=-1){
		printf("商品コード:%dの在庫数は、%dです。\n",in_code,zaiko);
	}else{
		printf("該当する商品はありません。\n");
	}
}





/*

Z:\clang\kadai06>kadai0608
C言語実習課題6-8 在庫数検索
商品コード ==> 37
商品コード:37の在庫数は、333です。
商品コード ==> 21
商品コード:21の在庫数は、123です。
商品コード ==> 85
商品コード:85の在庫数は、200です。
商品コード ==> 10
該当する商品はありません。
商品コード ==> 40
商品コード:40の在庫数は、650です。
商品コード ==> 68
商品コード:68の在庫数は、430です。
商品コード ==> 99

Z:\clang\kadai06>kadai0608
C言語実習課題6-8 在庫数検索
商品コード ==> 99

Z:\clang\kadai06>kadai0608
C言語実習課題6-8 在庫数検索
商品コード ==> 9
商品コードは、10から90です。
商品コード ==> 50
該当する商品はありません。
商品コード ==> 91
商品コードは、10から90です。
商品コード ==> 37
商品コード:37の在庫数は、333です。
商品コード ==> 99
*/