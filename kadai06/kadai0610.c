#include <stdio.h>
int authenticate(int code);

int main(void)
{
	int code,result;
	printf("C言語実習課題6-10 暗証番号確認\n");
	do{
		printf("暗証番号 ==> ");
		scanf("%d",&code);
		result=authenticate(code);
		switch(result){
			case 1:
				printf("暗証番号が一致しました。\n");
				break;
			case 0:
				printf("暗証番号不一致\n");
				break;
			case 9:
				printf("暗証番号不一致\nロックしました。\n");
		}
	}while(result==0);
	return 0;
}

int authenticate(int code)
{
	int password=1234,result;
	static int attempt=0;
	if(code==password){
		result=1;
	}else{
		attempt++;
		if(attempt<3){
			result=0;
		}else{
			result=9;
		}
	}
	return result;
}

/*
Z:\clang\kadai06>kadai0610
C言語実習課題6-10 暗証番号確認
暗証番号 ==> 1234
暗証番号が一致しました。

Z:\clang\kadai06>kadai0610
C言語実習課題6-10 暗証番号確認
暗証番号 ==> 1111
暗証番号不一致
暗証番号 ==> 5555
暗証番号不一致
暗証番号 ==> 1234
暗証番号が一致しました。

Z:\clang\kadai06>kadai0610
C言語実習課題6-10 暗証番号確認
暗証番号 ==> 2222
暗証番号不一致
暗証番号 ==> 3333
暗証番号不一致
暗証番号 ==> 4444
暗証番号不一致
ロックしました。
*/