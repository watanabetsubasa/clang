#include <stdio.h>
int hantei(int year,int month,int day);
int uru(int year);

int main(void)
{
	int year,month,day,result;
	printf("C言語実数課題6-6 年月日チェック\n");
	printf("年 ==> ");
	scanf("%d",&year);
	printf("月 ==> ");
	scanf("%d",&month);
	printf("日 ==> ");
	scanf("%d",&day);
	result=hantei(year,month,day);
	if(result==1){
		printf("%d年%d月%d日は正しい日付です。",year,month,day);
	}else{
		printf("%d年%d月%d日は誤った日付です。",year,month,day);
	}
	
	return 0;
}

int hantei(int year,int month,int day)
{
	int result=0,uru_result;
	if(1900<=year&&year<=2100){
		if(1<=month&&month<=12){
			if(month!=2){
				if(((month==1||month==3||month==5||month==7||month==8||month==10||month==12)&&(1<=day&&day<=31))||((month==4||month==6||month==9||month==11)&&(1<=day&&day<=30))){
					result=1;
				}
			}else{
				uru_result=uru(year);
				if(1<=day&&day<=28+uru_result){
					result=1;
				}
			}
		}
	}
	return result;
}

int uru(int year)
{
	int uru_result;
	if(year%400==0||(year%4==0&&year%100!=0)){
		uru_result=1;
	}else{
		uru_result=0;
	}
	return uru_result;
}

/*
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2024
月 ==> 2
日 ==> 29
2024年2月29日は正しい日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2023
月 ==> 2
日 ==> 29
2023年2月29日は誤った日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 1899
月 ==> 12
日 ==> 31
1899年12月31日は誤った日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 1900
月 ==> 1
日 ==> 1
1900年1月1日は正しい日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2100
月 ==> 12
日 ==> 31
2100年12月31日は正しい日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2101
月 ==> 1
日 ==> 1
2101年1月1日は誤った日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2020
月 ==> 13
日 ==> 31
2020年13月31日は誤った日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2021
月 ==> 0
日 ==> 1
2021年0月1日は誤った日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2022
月 ==> 5
日 ==> 31
2022年5月31日は正しい日付です。
Z:\clang\kadai06>kadai0606
C言語実数課題6-6 年月日チェック
年 ==> 2022
月 ==> 6
日 ==> 31
2022年6月31日は誤った日付です。
*/