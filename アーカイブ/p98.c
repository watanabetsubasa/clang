#include <stdio.h>
#include <ctype.h>

int main(void)
{
	int ch;
	/*ch=getchar();
	while(ch!=EOF){//windous10はctrl+Zを3回入力しENTERキーを押すと入力が終了する
		ch=toupper(ch);//toupper小文字を大文字に変換するライブラリ関数
		putchar(ch);
		ch=getchar();
	}*/
	while((ch=getchar())!=EOF){//!=は=より演算子の優先度が高いので(ch=getchar())となる。
		ch=toupper(ch);
		putchar(ch);
	}
	
	return 0;
}
