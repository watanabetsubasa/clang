#include <stdio.h>
#define MAXDATA 3719//セミコロン付けない

int main(void)
{
	int n8, n16;
	double f1, f2;
	int c;
	char ss[10] = "string";//文字列は""で囲む
	const int a = 10;
	
	n8 = 0100; //先頭を0にする
	n16 = 0x100;
	f1 = 1.234;
	f2 = 5.678e2;
	c='A';//文字を代入する場合、''で囲む
	
	printf("n8=%d\n",n8);
	printf("n16=%d\n",n16);
	printf("f1=%f\n",f1);
	printf("f2=%f\n",f2);
	printf("c=%c\n",c);
	printf("ss=%s\n",ss);//%sは文字列の変換指定P113
	
	printf("AAA\tBBB\tCCC\nDDD\tEEE\tFFF\n");
	printf("MAXDATA=%d\n",MAXDATA);
	
	printf("a=%d\n",a);
	//a = 15;//コンパイルエラーになる
	
	return 0;
}
