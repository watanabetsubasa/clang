#include <stdio.h>

int main(void){
	int idt;
	double ddt=123.456;
	double d1,d2=123.456;
	
	idt=(int)ddt;//小数ddtを整数型に変換する
	printf("idt=%d ddt=%f\n",idt,ddt);
	d1=(int)d2;
	printf("%f %f\n",d1,d2);
	
	return 0;
}
