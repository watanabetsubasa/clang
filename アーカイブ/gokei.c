#include <stdio.h>

int main(void)
{
	int tanka, suryo, kingaku, gokei, end_flag;
	gokei = 0;
	//printf("単価、数量を入力==>");
	end_flag = scanf("%d%d", &tanka, &suryo);
	while(end_flag != EOF){
		kingaku = tanka * suryo;
		gokei += kingaku;// P61 複合代入演算子 gokei = gokei + kingaku;
		printf("%4d %3d %5d\n", tanka, suryo, kingaku);
		//printf("単価、数量を入力==>");
		end_flag = scanf("%d%d", &tanka, &suryo);
	}
	printf("総合計 = %5d\n", gokei);
	return 0;
}
