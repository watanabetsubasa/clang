#include <stdio.h>

int main(void){
	int a[4]={11,12,13,14};
	int b[]={21,22,23,24};
	int c[4]={31,32};
	double d[4]={1.1,2.2,3.3,4.4};
	char s1[10]="abcde";
	char s2[]="ABCDE";
	
	
	printf("%d\n",a[0]);
	printf("%d\n",a[3]);
	printf("%d\n",b[0]);
	printf("%d\n",b[3]);
	printf("%d\n",c[0]);
	printf("%d\n",c[2]);
	printf("%f\n",d[0]);
	printf("%f\n",d[3]);
	printf("%s\n",s1);
	printf("%s\n",s2);
	
	return 0;
}
