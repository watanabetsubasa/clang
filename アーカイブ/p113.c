#include <stdio.h>

int main(void)
{
	char c;
	int n;
	long l;
	double d=654.321;
	
	c= 65;
	printf("c: %-8c\n",c);
	
	n=1000;
	printf("8: %o\n",n);
	printf("16x: %8x\n",n);
	printf("16X: %08X\n",n);
	
	l=1234567890;
	printf("ld: %ld\n",l);
	
	printf(":%12f:\n",d);
	printf(":%9.2f:\n",d);
	printf(":%9.f:\n",d);
	printf(":%.2f:\n",d);
	return 0;
}
