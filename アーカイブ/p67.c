#include <stdio.h>

int main(void){
	int a,b[10];
	
	printf("%zd\n",sizeof a);
	printf("%zd\n",sizeof b);
	printf("%zd\n",sizeof(char));
	printf("%zd\n",sizeof(short int));
	printf("%zd\n",sizeof(int));
	printf("%zd\n",sizeof(long int));
	printf("%zd\n",sizeof(float));
	printf("%zd\n",sizeof(double));
	printf("%zd\n",sizeof(a));
	printf("%zd\n",sizeof(b));
//	printf("%d\n",sizeof char);データ型は括弧をつけること
//	printf("%d\n",sizeof short int);
	return 0;
}
