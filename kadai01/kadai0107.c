#include <stdio.h>

int main(void)
{
	printf("C言語実習課題1-7 商品一覧表\n");
	int mikan, ringo, banana, mikan_kingaku, ringo_kingaku, banana_kingaku, gokei;
	printf("みかん ==> ");
	scanf("%d", &mikan);
	printf("りんご ==> ");
	scanf("%d", &ringo);
	printf("バナナ ==> ");
	scanf("%d", &banana);
	mikan_kingaku = 50 * mikan;
	ringo_kingaku = 80 * ringo;
	banana_kingaku = 120 * banana;
	gokei = mikan_kingaku +ringo_kingaku + banana_kingaku;
	printf("  *** 商品一覧表 ***\n商品名 単価 数量  金額\n------ ---- ---- -----\n");
	printf("みかん   50 %4d %5d\n", mikan, mikan_kingaku);
	printf("りんご   80 %4d %5d\n", ringo, ringo_kingaku);
	printf("バナナ  120 %4d %5d\n", banana, banana_kingaku);
	printf("        合計金額 %5d\n", gokei);
	return 0;
}

/*
	○実行結果
	Z:\clang\kadai01>kadai0107
	C言語実習課題1-7 商品一覧表
	みかん ==> 5
	りんご ==> 6
	バナナ ==> 7
	  *** 商品一覧表 ***
	商品名 単価 数量  金額
	------ ---- ---- -----
	みかん   50    5   250
	りんご   80    6   480
	バナナ  120    7   840
	        合計金額  1570



	Z:\clang\kadai01>kadai0107
	C言語実習課題1-7 商品一覧表
	みかん ==> 40
	りんご ==> 30
	バナナ ==> 50
	  *** 商品一覧表 ***
	商品名 単価 数量  金額
	------ ---- ---- -----
	みかん   50   40  2000
	りんご   80   30  2400
	バナナ  120   50  6000
	        合計金額 10400
        
*/