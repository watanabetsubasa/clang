#include <stdio.h>

int main(void)
{
	printf( "C言語実習課題1-3 四則演算\n" );
	int seisu1, seisu2, wa, sa, seki, sho, amari;
	printf("1つ目の整数を入力してください　==> ");
	scanf("%d", &seisu1);
	printf("2つ目の整数を入力してください　==> ");
	scanf("%d", &seisu2);
	wa = seisu1 + seisu2;
	sa = seisu1 - seisu2;
	seki = seisu1 * seisu2;
	sho = seisu1 / seisu2;
	amari = seisu1 % seisu2;
	printf("%d + %d = %d\n", seisu1, seisu2, wa);
	printf("%d - %d = %d\n", seisu1, seisu2, sa);
	printf("%d * %d = %d\n", seisu1, seisu2, seki);
	printf("%d / %d = %d\n", seisu1, seisu2, sho);
	printf("%d %% %d = %d\n", seisu1, seisu2, amari);
	return 0;
}

/*
	○実行結果
	
	Z:\clang\kadai01>kadai0103
	C言語実習課題1-3 四則演算
	1つ目の整数を入力してください　==> 30
	2つ目の整数を入力してください　==> 20
	30 + 20 = 50
	30 - 20 = 10
	30 * 20 = 600
	30 / 20 = 1
	30 % 20 = 10

	Z:\clang\kadai01>kadai0103
	C言語実習課題1-3 四則演算
	1つ目の整数を入力してください　==> 500
	2つ目の整数を入力してください　==> 150
	500 + 150 = 650
	500 - 150 = 350
	500 * 150 = 75000
	500 / 150 = 3
	500 % 150 = 50
	
*/