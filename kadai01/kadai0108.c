#include <stdio.h>

int main(void)
{
	int ymd, year, month, day;
	printf("C言語実習課題 1-8 年月日編集\n");
	printf("年月日(8桁) ==>	");
	scanf("%d", &ymd);
	year = ymd / 10000;
	month = (ymd % 10000) / 100;
	day = ymd % 100;
	printf("%d年 %d月 %d日です。\n", year, month, day);
	return 0;
}

/*
	○結果
	
	Z:\clang\kadai01>kadai0108
	C言語実習課題 1-8 年月日編集
	年月日(8桁) ==> 20201205
	2020年 12月 5日です。

	Z:\clang\kadai01>kadai0108
	C言語実習課題 1-8 年月日編集
	年月日(8桁) ==> 20211010
	2021年 10月 10日です。
	
	Z:\clang\kadai01>kadai0108
	C言語実習課題 1-8 年月日編集
	年月日(8桁) ==> 20200405
	2020年 4月 5日です。

*/