#include <stdio.h>

int main(void)
{
	char upper, lower, mae, ato;
	printf("アルファベット大文字　==> ");
	scanf("%c", &upper);
	lower = upper + 32;
	mae = upper - 1;
	ato = upper + 1;
	printf("%c の小文字は %c です。\n", upper, lower);
	printf("%c のひとつ前は %c です。\n", upper, mae);
	printf("%c のひとつ後は %c です。\n", upper, ato);
	printf("%c を10進数で表すと %d です。\n", upper, upper);
	printf("%c を16進数で表すと %X です。\n", upper, upper);
	return 0;
}

/*
	○結果
	Z:\clang\kadai01>kadai0109
	アルファベット大文字　==> M
	M の小文字は m です。
	M のひとつ前は L です。
	M のひとつ後は N です。
	M を10進数で表すと 77 です。
	M を16進数で表すと 4D です。

	Z:\clang\kadai01>kadai0109
	アルファベット大文字　==> B
	B の小文字は b です。
	B のひとつ前は A です。
	B のひとつ後は C です。
	B を10進数で表すと 66 です。
	B を16進数で表すと 42 です。
*/
