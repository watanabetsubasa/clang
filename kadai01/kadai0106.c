#include <stdio.h>

int main(void)
{
	printf("C言語実習課題1-6 円の面積と球の体積\n");
	int hankei;
	double menseki, taiseki;
	printf("半径 ==> ");
	scanf("%d", &hankei);
	menseki = hankei * hankei * 3.14;
	taiseki= 4 / 3.0 * hankei * hankei * hankei * 3.14;
	printf("半径 %d の円の面積は %.1lf です。\n", hankei, menseki);
	printf("半径 %d の円の体積は %.1lf です。\n ", hankei, taiseki);
	return 0;
}

/*
	○実行結果
	
	Z:\clang\kadai01>kadai0106
	C言語実習課題1-6 円の面積と球の体積
	半径 ==> 5
	半径 5 の円の面積は 78.5 です。
	半径 5 の円の体積は 523.3 です。

	Z:\clang\kadai01>kadai0106
	C言語実習課題1-6 円の面積と球の体積
	半径 ==> 10
	半径 10 の円の面積は 314.0 です。
	半径 10 の円の体積は 4186.7 です。
	
*/