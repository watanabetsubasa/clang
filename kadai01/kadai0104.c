#include <stdio.h>

int main(void)
{
	printf("C言語実習課題1-4 注文数\n");
	int suryo, kesu, kobetsu;
	printf("必要数量 ==> ");
	scanf("%d", &suryo);
	kesu = suryo / 24;
	kobetsu = suryo % 24;
	printf("%dケースと、%d個注文する。\n", kesu, kobetsu);
	return 0;
}

/*
	○実行結果
	Z:\clang\kadai01>kadai0104
	C言語実習課題1-4 注文数
	必要数量 ==> 80
	3ケースと、8個注文する。
	
	Z:\clang\kadai01>kadai0104
	C言語実習課題1-4 注文数
	必要数量 ==> 120
	5ケースと、0個注文する。
	
*/