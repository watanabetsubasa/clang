#include <stdio.h>

int main(void)
{
	int seisu,i,sum=0;
	printf("C言語実習課題 3-1 1から入力値までの合計\n");
	printf("整数 ==> ");
	scanf("%d",&seisu);
	for(i=1;i<=seisu;i++){
		sum+=i;
	}
	printf("1 から %d までの合計は %d です。\n",seisu,sum);
	return 0;
}
/*
	Z:\clang\kadai03>kadai0301
	C言語実習課題 3-1 1から入力値までの合計
	整数 ==> 1
	1 から 1 までの合計は 1 です。

	Z:\clang\kadai03>kadai0301
	C言語実習課題 3-1 1から入力値までの合計
	整数 ==> 25
	1 から 25 までの合計は 325 です。

	Z:\clang\kadai03>kadai0301
	C言語実習課題 3-1 1から入力値までの合計
	整数 ==> 65535
	1 から 65535 までの合計は 2147450880 です。
*/