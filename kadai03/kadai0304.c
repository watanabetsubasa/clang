#include <stdio.h>

int main(void){
	int point;
	printf("C言語実習課題 3-4 合否判定\n");
	printf("点数 ==> ");
	scanf("%d",&point);
	while(point<0 || 100<point){
	printf("点数は0から100を入力して下さい\n点数 ==> ");
	scanf("%d",&point);
	}
	if(point>=60){
		printf("合格\n");
	}else{
		printf("不合格\n");
	}
	return 0;
}

/*
	Z:\clang\kadai03>kadai0304
	C言語実習課題 3-4 合否判定
	点数 ==> 300
	点数は0から100を入力して下さい
	点数 ==> -123
	点数は0から100を入力して下さい
	点数 ==> 100
	合格

	Z:\clang\kadai03>kadai0304
	C言語実習課題 3-4 合否判定
	点数 ==> 59
	不合格

	Z:\clang\kadai03>kadai0304
	C言語実習課題 3-4 合否判定
	点数 ==> 101
	点数は0から100を入力して下さい
	点数 ==> -1
	点数は0から100を入力して下さい
	点数 ==> 101
	点数は0から100を入力して下さい
	点数 ==> 60
	合格

	Z:\clang\kadai03>kadai0304
	C言語実習課題 3-4 合否判定
	点数 ==> -200
	点数は0から100を入力して下さい
	点数 ==> 300
	点数は0から100を入力して下さい
	点数 ==> 0
	不合格
	
*/
	