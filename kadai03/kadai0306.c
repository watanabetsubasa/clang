#include <stdio.h>

int main(void){
	int seisu,kaijo=1,i;
	printf("C言語実習課題 3-6 階乗\n");
	printf("整数 ==> ");
	scanf("%d",&seisu);
	for(i=seisu;i>0;i--){
		kaijo=kaijo*i;
	}
	printf("%dの階乗は%dです。\n",seisu,kaijo);
	return 0;
}
/*
	Z:\clang\kadai03>kadai0306
	C言語実習課題 3-6 階乗
	整数 ==> 5
	5の階乗は120です。

	Z:\clang\kadai03>kadai0306
	C言語実習課題 3-6 階乗
	整数 ==> 1
	1の階乗は1です。

	Z:\clang\kadai03>kadai0306
	C言語実習課題 3-6 階乗
	整数 ==> 0
	0の階乗は1です。

	Z:\clang\kadai03>kadai0306
	C言語実習課題 3-6 階乗
	整数 ==> 12
	12の階乗は479001600です。	
/*