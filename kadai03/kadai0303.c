#include <stdio.h>

int main(void){
	int tate,yoko,i,j;
	printf("C言語実習課題 3-3 長方形\n");
	printf("たて ==> ");
	scanf("%d",&tate);
	printf("よこ ==> ");
	scanf("%d",&yoko);
	for(i=1;i<=tate;i++){
		for(j=1;j<=yoko;j++){
			printf("*");
		}
		printf("\n");
	}
	return 0;
}

/*
	Z:\clang\kadai03>kadai0303
	C言語実習課題 3-3 長方形
	たて ==> 1
	よこ ==> 1
	*

	Z:\clang\kadai03>kadai0303
	C言語実習課題 3-3 長方形
	たて ==> 1
	よこ ==> 5
	*****

	Z:\clang\kadai03>kadai0303
	C言語実習課題 3-3 長方形
	たて ==> 5
	よこ ==> 1
	*
	*
	*
	*
	*


	Z:\clang\kadai03>kadai0303
	C言語実習課題 3-3 長方形
	たて ==> 3
	よこ ==> 7
	*******
	*******
	*******
*/
	