#include <stdio.h>

int main(void){
	int seisu,i,max,min;
	printf("C言語実習課題 3-5 最大値と最小値\n");
	printf("整数 ==> ");
	scanf("%d",&seisu);
	max=seisu;
	min=seisu;
	for(i=0;i<4;i++){
		printf("整数 ==> ");
		scanf("%d",&seisu);
		if(seisu>max){
			max=seisu;
		}
		if(seisu<min){
			min=seisu;
		}
	}
	printf("最大値 = %d\n最小値 = %d\n",max,min);
	return 0;
}

/*
	Z:\clang\kadai03>kadai0305
	C言語実習課題 3-5 最大値と最小値
	整数 ==> 5
	整数 ==> -100
	整数 ==> -70
	整数 ==> 200
	整数 ==> 60
	最大値 = 200
	最小値 = -100

	Z:\clang\kadai03>kadai0305
	C言語実習課題 3-5 最大値と最小値
	整数 ==> -80
	整数 ==> -3
	整数 ==> 0
	整数 ==> 230
	整数 ==> 500
	最大値 = 500
	最小値 = -80

	Z:\clang\kadai03>kadai0305
	C言語実習課題 3-5 最大値と最小値
	整数 ==> 70
	整数 ==> 60
	整数 ==> 50
	整数 ==> 40
	整数 ==> 30
	最大値 = 70
	最小値 = 30
*/
	