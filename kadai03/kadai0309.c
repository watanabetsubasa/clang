#include <stdio.h>

int main(void)
{
	int height,length,i,j,k;
	printf("C言語実習課題 3-9 三角形\n");
	printf("三角形の高さ ==> ");
	scanf("%d",&height);
	while(!(2<=height && height<=5)){
		printf("高さは2から5の整数で入力してください。\n");
		printf("三角形の高さ ==> ");
		scanf("%d",&height);
	}
	for(i=0;i<height;i++){
		length=2*i+1;
		for (j=0;j<=height-i-2;j++){
      		printf(" ");
		}
		for(k=0;k<length;k++){
			 printf("*");
		}
		printf("\n");
	}
	return 0;
}
/*
	Z:\clang\kadai03>kadai0309
	C言語実習課題 3-9 三角形
	三角形の高さ ==> 2
	 *
	***
	Z:\clang\kadai03>kadai0309
	C言語実習課題 3-9 三角形
	三角形の高さ ==> 5
	    *
	   ***
	  *****
	 *******
	*********

	Z:\clang\kadai03>kadai0309
	C言語実習課題 3-9 三角形
	三角形の高さ ==> 100
	高さは2から5の整数で入力してください。
	三角形の高さ ==> 3
	  *
	 ***
	*****

	Z:\clang\kadai03>kadai0309
	C言語実習課題 3-9 三角形
	三角形の高さ ==> 1
	高さは2から5の整数で入力してください。
	三角形の高さ ==> 6
	高さは2から5の整数で入力してください。
	三角形の高さ ==> 10
	高さは2から5の整数で入力してください。
	三角形の高さ ==> 4
	   *
	  ***
	 *****
	*******
	
	*/