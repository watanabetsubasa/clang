#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

struct product{
	int code;
	char name[11];
	int unitPrice;
};

int main(void)
{
	FILE *fp3,*fp_t,*fp4;
	int end_flag3,end_flag_t,unchangedRecs,addedRecs,changedRecs,deletedRecs;
	char t_kubun[10];
	struct product data3,data_t,data4;
	
	printf("C言語実習課題10-7 マッチング\n");
	
	fp3=fopen("syohin3.mst","r");
	if(fp3==NULL){
		printf("商品マスタ3が存在しません。\n");
		exit(EXIT_FAILURE);
	}
	
	fp_t=fopen("henko.trn","r");
	if(fp_t==NULL){
		printf("変更ファイルが存在しません。\n");
		fclose(fp_t);
		exit(EXIT_FAILURE);
	}
	
	fp4=fopen("syohin4.mst","w");
	if(fp4==NULL){
		printf("商品マスタ4が作成できません。\n");
		fclose(fp3);
		fclose(fp_t);
		exit(EXIT_FAILURE);
	}
	
	end_flag3=fscanf(fp3,"%d%s%d",&data3.code,data3.name,&data3.unitPrice);
	if(end_flag3==EOF){
		data3.code=INT_MAX;
	}
	
	end_flag_t=fscanf(fp_t,"%s%d%s%d",t_kubun,&data_t.code,data_t.name,&data_t.unitPrice);
	if(end_flag_t==EOF){
		data_t.code=INT_MAX;
	}
	
	unchangedRecs=0;
	addedRecs=0;
	changedRecs=0;
	deletedRecs=0;
	
	while(data3.code!=INT_MAX||data_t.code!=INT_MAX){
		if(data3.code<data_t.code){
			data4=data3;
			fprintf(fp4,"%2d %-10s %3d\n",data4.code,data4.name,data4.unitPrice);
			unchangedRecs++;
			end_flag3=fscanf(fp3,"%d%s%d",&data3.code,data3.name,&data3.unitPrice);
			if(end_flag3==EOF){
				data3.code=INT_MAX;
			}
		}else{
			if(data3.code==data_t.code){
				if(strcmp(t_kubun,"UP")==0){
					data4.code=data3.code;
					strcpy(data4.name,data3.name);
					data4.unitPrice=data_t.unitPrice;
					fprintf(fp4,"%2d %-10s %3d\n",data4.code,data4.name,data4.unitPrice);
					changedRecs++;
				}else{
					deletedRecs++;
				}
				end_flag3=fscanf(fp3,"%d%s%d",&data3.code,data3.name,&data3.unitPrice);
				if(end_flag3==EOF){
					data3.code=INT_MAX;
				}
				
				end_flag_t=fscanf(fp_t,"%s%d%s%d",t_kubun,&data_t.code,data_t.name,&data_t.unitPrice);
				if(end_flag_t==EOF){
					data_t.code=INT_MAX;
				}
			}else{
				data4=data_t;
				fprintf(fp4,"%2d %-10s %3d\n",data4.code,data4.name,data4.unitPrice);
				addedRecs++;
				
				end_flag_t=fscanf(fp_t,"%s%d%s%d",t_kubun,&data_t.code,data_t.name,&data_t.unitPrice);
				if(end_flag_t==EOF){
					data_t.code=INT_MAX;
				}
			}
		}
	}
	
	
	
	printf("変更なし：　%d 件\n",unchangedRecs);
	printf("追加処理：　%d 件\n",addedRecs);
	printf("変更処理：　%d 件\n",changedRecs);
	printf("削除処理：　%d 件\n",deletedRecs);
	
	fclose(fp4);
	fclose(fp_t);
	fclose(fp3);
	
	return 0;
}
/*
Z:\clang\kadai10>type syohin3.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
74 分度器     370
85 コンパス   230

Z:\clang\kadai10>type henko.trn
AD 20 下敷き      110
UP 21 ノート      120
DL 37 定規        150
AD 53 三角定規    300
UP 85 コンパス    260

Z:\clang\kadai10>type syohin4.mst
指定されたファイルが見つかりません。

Z:\clang\kadai10>kadai1007
C言語実習課題10-7 マッチング
変更なし：　4 件
追加処理：　2 件
変更処理：　2 件
削除処理：　1 件

Z:\clang\kadai10>type syohin4.mst
16 鉛筆        30
20 下敷き     110
21 ノート     120
40 消しゴム    50
53 三角定規   300
63 ボールペン  80
74 分度器     370
85 コンパス   260
*/