#include <stdio.h>
#include <stdlib.h>

struct product{
	int code;
	char name[11];
	int unitPrice;
};

int main(void)
{
	
	FILE *fp;
	int in_code,end_flag;
	struct product data;
	
	printf("C言語実習課題10-10 商品検索\n");
	
	fp=fopen("syohin5.mst","rb");
	if(fp==NULL){
		printf("商品マスタが存在しません\n");
		exit(EXIT_FAILURE);
	}
	
	printf("商品コード ==> ");
	scanf("%d",&in_code);
	
	while(in_code!=99){
		end_flag=fread(&data,sizeof(struct product),1,fp);
		while(end_flag!=0&&in_code!=data.code){
			end_flag=fread(&data,sizeof(struct product),1,fp);
		}
		
		if(end_flag==0){
			printf("該当する商品はありません。\n");
		}else{
			printf("商品名 : %s\n単  価 : %d\n",data.name,data.unitPrice);
		}
	
		rewind(fp);
		
		printf("商品コード ==> ");
		scanf("%d",&in_code);
	}
	
	fclose(fp);
	
	return 0;
}



/*
Z:\clang\kadai10>kadai1010
C言語実習課題10-10 商品検索
商品コード ==> 85
商品名 : コンパス
単  価 : 260
商品コード ==> 43
該当する商品はありません。
商品コード ==> 21
商品名 : ノート
単  価 : 120
商品コード ==> 99

Z:\clang\kadai10>kadai1010
C言語実習課題10-10 商品検索
商品コード ==> 99
*/