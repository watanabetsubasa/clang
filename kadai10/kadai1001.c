#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *fp;
	
	int end_flag,code,price;
	char name[11];
	
	printf("C言語実習課題10-1 商品一覧表\n");
	
	fp=fopen("shohin.mst","r");
	if(fp==NULL){
		printf("商品マスタ(syohin.mst)がみつかりません。\n");
		exit(EXIT_FAILURE);
	}
	
	printf("  *** 商品一覧表 ***\nコード 商　品　名 単価\n------ ---------- ----\n");
	
	end_flag=fscanf(fp,"%d%s%d",&code,name,&price);
	while(end_flag!=EOF){
		printf("%6d %-10s %4d\n",code,name,price);
		end_flag=fscanf(fp,"%d%s%d",&code,name,&price);
	}
	
	fclose(fp);
	
	return 0;
}
/*
Z:\clang\kadai10>kadai1001
C言語実習課題10-1 商品一覧表
  *** 商品一覧表 ***
コード 商　品　名 単価
------ ---------- ----
    16 鉛筆         30
    21 ノート      100
    37 定規        150
    40 消しゴム     50
    63 ボールペン   80

Z:\clang\kadai10>kadai1001
C言語実習課題10-1 商品一覧表
商品マスタ(syohin.mst)がみつかりません。
*/