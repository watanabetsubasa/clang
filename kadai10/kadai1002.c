#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *fp_i,*fp_o;
	int end_flag,code,price,i;
	char name[11],f_i[20],f_o[20];
	
	printf("C言語実習課題10-2 ファイルコピー\n");
	
	printf("入力ファイル名 ==> ");
	scanf("%s",f_i);
	printf("出力ファイル名 ==> ");
	scanf("%s",f_o);
	
	fp_i=fopen(f_i,"r");
	if(fp_i==NULL){
		printf("入力ファイル(%s)がみつかりません。\n",f_i);
		exit(EXIT_FAILURE);
	}
	
	fp_o=fopen(f_o,"w");
	if(fp_o==NULL){
		fclose(fp_i);
		printf("出力ファイル(%s)が作成できません。",f_o);
		exit(EXIT_FAILURE);
	}
	
	printf("*** ファイルコピー開始 ***\n");
	
	end_flag=fscanf(fp_i,"%d%s%d",&code,name,&price);
	for(i=0;end_flag!=EOF;i++){
		fprintf(fp_o,"%2d %-10s %3d\n",code,name,price);
		end_flag=fscanf(fp_i,"%d%s%d",&code,name,&price);
	}
	
	printf("%d レコードをコピーしました。\n",i);
	printf("*** ファイルコピー終了 ***\n");
	
	fclose(fp_i);
	fclose(fp_o);
	
	return 0;
}



/*
Z:\clang\kadai10>kadai1002
C言語実習課題10-2 ファイルコピー
入力ファイル名 ==> syohin.mst
出力ファイル名 ==> syohin.bak
*** ファイルコピー開始 ***
5 レコードをコピーしました。
*** ファイルコピー終了 ***

Z:\clang\kadai10>kadai1002
C言語実習課題10-2 ファイルコピー
入力ファイル名 ==> tokuisaki.mst
出力ファイル名 ==> tokuisaki.bak
入力ファイル(tokuisaki.mst)がみつかりません。

Z:\clang\kadai10>type syohin.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
Z:\clang\kadai10>type syohin.bak
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80

*/