#include <stdio.h>
#include <stdlib.h>

struct product{
	int code;
	char name[11];
	int price;
};

int main(void)
{
	FILE *fp;
	int in_code,end_flag;
	struct product data;
	
	printf("C言語実習課題10-4 商品検索\n");	
	
	fp=fopen("syohin.mst","r");
	if(fp==NULL){
		printf("商品マスタが存在しません。\n");
		exit(EXIT_FAILURE);
	}
	
	printf("商品コード ==> ");
	scanf("%d",&in_code);
	
	while(in_code!=99){
		end_flag=fscanf(fp,"%d%s%d",&data.code,data.name,&data.price);
		while(end_flag!=EOF&&in_code!=data.code){
			end_flag=fscanf(fp,"%d%s%d",&data.code,data.name,&data.price);
		}
		
		if(end_flag==EOF){
			printf("該当する商品はありません。\n");
		}else{
			printf("商品名 : %s\n単価 : %d\n",data.name,data.price);
		}
		
		rewind(fp);
		
		printf("商品名 ==> ");
		scanf("%d",&in_code);
	}
	
	fclose(fp);
	
	return 0;
}

/*
Z:\clang\kadai10>type syohin.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
74 分度器     370
85 コンパス   230

Z:\clang\kadai10>kadai1004
C言語実習課題10-4 商品検索
商品コード ==> 85
商品名 : コンパス
単価 : 230
商品名 ==> 43
該当する商品はありません。
商品名 ==> 21
商品名 : ノート
単価 : 100
商品名 ==> 85
商品名 : コンパス
単価 : 230
商品名 ==> 99

Z:\clang\kadai10>kadai1004
C言語実習課題10-4 商品検索
商品コード ==> 99