#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *fp;
	int code,price;
	char name[11];
	
	printf("C言語実習課題10-1 商品一覧表\n");
	
	fp=fopen("syohin.mst","a");
	if(fp==NULL){
		printf("商品マスタ(syohin.mst)が作成できません。\n");
		exit(EXIT_FAILURE);
	}
	
	printf("商品マスタに１レコード追加します。\n");
	printf("商品コード ==> ");
	scanf("%d",&code);
	printf("　　商品名 ==> ");
	scanf("%s",name);
	printf("　　　単価 ==> ");
	scanf("%d",&price);
	fprintf(fp,"%2d %-10s %3d\r\n",code,name,price);
	
	fclose(fp);
	printf("商品マスタに１レコード追加しました。\n");
	
	return 0;
}
/*
Z:\clang\kadai10>type syohin.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80

Z:\clang\kadai10>kadai1003
C言語実習課題10-1 商品一覧表
商品マスタに１レコード追加します。
商品コード ==> 74
　　商品名 ==> 分度器
　　　単価 ==> 370
商品マスタに１レコード追加しました。

Z:\clang\kadai10>type syohin.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
74 分度器     370

Z:\clang\kadai10>kadai1003
C言語実習課題10-1 商品一覧表
商品マスタに１レコード追加します。
商品コード ==> 85
　　商品名 ==> コンパス
　　　単価 ==> 230
商品マスタに１レコード追加しました。

Z:\clang\kadai10>type syohin.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
74 分度器     370
85 コンパス   230
*/