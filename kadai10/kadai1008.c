#include <stdio.h>
#include <stdlib.h>

struct product{
	int code;
	char name[11];
	int unitPrice;
};

int main(void)
{
	FILE *fp_i,*fp_o;
	int end_flag;
	struct product data;
	
	printf("C言語実習課題10-8 バイナリファイル生成\n");
	
	fp_i=fopen("syohin4.mst","r");
	if(fp_i==NULL){
		printf("入力ファイル(syohin4.mst)がみつかりません。\n");
		exit(EXIT_FAILURE);
	}
	
	fp_o=fopen("syohin5.mst","wb");
	if(fp_o==NULL){
		fclose(fp_i);
		printf("商品マスタ5が作成できません。\n");
		exit(EXIT_FAILURE);
	}
	
	printf("\n*** バイナリファイル生成 開始 ***\n");
	
	end_flag=fscanf(fp_i,"%d%s%d",&data.code,data.name,&data.unitPrice);
	while(end_flag!=EOF){
		fwrite(&data,sizeof(struct product),1,fp_o);
		end_flag=fscanf(fp_i,"%d%s%d",&data.code,data.name,&data.unitPrice);
	}
	
	printf("\n*** バイナリファイル生成 完了 ***\n");
	
	fclose(fp_i);
	fclose(fp_o);
	
	return 0;
}



/*
Z:\clang\kadai10>type syohin4.mst
16 鉛筆        30
20 下敷き     110
21 ノート     120
40 消しゴム    50
53 三角定規   300
63 ボールペン  80
74 分度器     370
85 コンパス   260

Z:\clang\kadai10>kadai1008
C言語実習課題10-8 ファイルコピー

*** バイナリファイル生成 開始 ***

*** バイナリファイル生成 完了 ***
*/