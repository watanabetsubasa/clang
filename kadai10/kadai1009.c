#include <stdio.h>
#include <stdlib.h>

struct product{
	int code;
	char name[11];
	int unitPrice;
};

int main(void)
{
	
	FILE *fp;
	struct product data;
	int end_flag;
	
	printf("C言語実習課題10-9 商品一覧表\n");
	
	fp=fopen("syohin5.mst","rb");
	if(fp==NULL){
		printf("商品マスタ5が存在しません\n");
		exit(EXIT_FAILURE);
	}
	
	printf("\n  *** 商品一覧表 ***\nコード 商  品  名 単価\n------ ---------- ----\n");
	end_flag=fread(&data,sizeof(struct product),1,fp);
	while(end_flag!=0){
		printf("%6d %-10s %4d\n",data.code,data.name,data.unitPrice);
		end_flag=fread(&data,sizeof(struct product),1,fp);
	}
	
	fclose(fp);
	
	return 0;
}
/*
Z:\clang\kadai10>kadai1009
C言語実習課題10-9 商品一覧表

  *** 商品一覧表 ***
コード 商  品  名 単価
------ ---------- ----
    16 鉛筆         30
    20 下敷き      110
    21 ノート      120
    40 消しゴム     50
    53 三角定規    300
    63 ボールペン   80
    74 分度器      370
    85 コンパス    260
*/