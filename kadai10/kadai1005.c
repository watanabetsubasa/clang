#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct product{
	int slip;
	int code;
	char name[11];
	int unitPrice;
	int amount;
};

int main(void)
{
	FILE *fp;
	int end_flag,total,subtotal,save,price;
	struct product data;
	
	printf("C言語実習課題10-5 グループトータル\n");	
	
	fp=fopen("uriage.dat","r");
	if(fp==NULL){
		printf("売上データが存在しません。\n");
		exit(EXIT_FAILURE);
	}
	
	printf("\n　　　　　*** 売上一覧表 ***\n伝票 コード 商　品　名 単価 数量 　金額\n---- ------ ---------- ---- ---- ------\n");
	
	end_flag=fscanf(fp,"%d%d%s%d%d",&data.slip,&data.code,data.name,&data.unitPrice,&data.amount);
	if(end_flag==EOF){
		data.code=INT_MAX;
	}
	
	for(total=0;data.code!=INT_MAX;total+=subtotal){
		for(save=data.code,subtotal=0;data.code==save;){
			price=data.unitPrice*data.amount;
			printf("%4d %6d %-10s %4d %4d %6d\n",data.slip,data.code,data.name,data.unitPrice,data.amount,price);
			subtotal+=price;
			
			end_flag=fscanf(fp,"%d%d%s%d%d",&data.slip,&data.code,data.name,&data.unitPrice,&data.amount);
			if(end_flag==EOF){
				data.code=INT_MAX;
			}
		}
		printf("　　　　　　　　　*** 商品計 *** %6d\n",subtotal);
	}
	
	printf("　　　　　　　　　*** 総合計 *** %6d\n",total);
	
	fclose(fp);
	
	return 0;
}



/*
Z:\clang\kadai10>type uriage.dat
1001 16 鉛筆       30  50
1002 16 鉛筆       30 120
1003 16 鉛筆       30   6
1004 21 ノート    100  15
1005 21 ノート    100  66
1006 21 ノート    100 108
1007 21 ノート    100   3
1008 37 定規      150  20
1009 37 定規      150 200
1010 40 消しゴム   50  30
1011 40 消しゴム   50 150
1012 40 消しゴム   50   8

Z:\clang\kadai10>kadai1005
C言語実習課題10-5 グループトータル

　　　　　*** 売上一覧表 ***
伝票 コード 商　品　名 単価 数量 　金額
---- ------ ---------- ---- ---- ------
1001     16 鉛筆         30   50   1500
1002     16 鉛筆         30  120   3600
1003     16 鉛筆         30    6    180
　　　　　　　　　*** 商品計 ***   5280
1004     21 ノート      100   15   1500
1005     21 ノート      100   66   6600
1006     21 ノート      100  108  10800
1007     21 ノート      100    3    300
　　　　　　　　　*** 商品計 ***  19200
1008     37 定規        150   20   3000
1009     37 定規        150  200  30000
　　　　　　　　　*** 商品計 ***  33000
1010     40 消しゴム     50   30   1500
1011     40 消しゴム     50  150   7500
1012     40 消しゴム     50    8    400
　　　　　　　　　*** 商品計 ***   9400
　　　　　　　　　*** 総合計 ***  66880
*/