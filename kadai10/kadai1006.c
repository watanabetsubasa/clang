#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int main(void)
{
	FILE *fp1,*fp2,*fp3;
	int end_flag1,end_flag2,code1,code2,unitPrice1,unitPrice2,record1,record2;
	char name1[11],name2[11];
	
	printf("C言語実習課題10-6 マージ\n");
	
	fp1=fopen("syohin1.mst","r");
	if(fp1==NULL){
		printf("商品マスタ1が存在しません。\n");
		exit(EXIT_FAILURE);
	}
	
	fp2=fopen("syohin2.mst","r");
	if(fp2==NULL){
		printf("商品マスタ2が存在しません。\n");
		fclose(fp1);
		exit(EXIT_FAILURE);
	}
	
	fp3=fopen("syohin3.mst","w");
	if(fp3==NULL){
		printf("商品マスタが作成できません。\n");
		fclose(fp2);
		fclose(fp1);
		exit(EXIT_FAILURE);
	}
	
	end_flag1=fscanf(fp1,"%d%s%d",&code1,name1,&unitPrice1);
	if(end_flag1==EOF){
		code1=INT_MAX;
	}
	
	end_flag2=fscanf(fp2,"%d%s%d",&code2,name2,&unitPrice2);
	if(end_flag2==EOF){
		code2=INT_MAX;
	}
	
	record1=0;
	record2=0;
	
	while(code1!=INT_MAX||code2!=INT_MAX){
		if(code1<code2){
			fprintf(fp3,"%2d %-10s %3d\n",code1,name1,unitPrice1);
			record1++;
			end_flag1=fscanf(fp1,"%d%s%d",&code1,name1,&unitPrice1);
			if(end_flag1==EOF){
				code1=INT_MAX;
			}
		}else{
			fprintf(fp3,"%2d %-10s %3d\n",code2,name2,unitPrice2);
			record2++;
			end_flag2=fscanf(fp2,"%d%s%d",&code2,name2,&unitPrice2);
			if(end_flag2==EOF){
				code2=INT_MAX;
			}
		}
	}
	
	printf("入力ファイル(商品マスタ１)：　%d レコード\n",record1);
	printf("入力ファイル(商品マスタ２)：　%d レコード\n",record2);
	printf("出力ファイル(商品マスタ３)：　%d レコード\n",record1+record2);
	
	fclose(fp3);
	fclose(fp2);
	fclose(fp1);
	
	return 0;
}
/*
Z:\clang\kadai10>type syohin1.mst
16 鉛筆        30
21 ノート     100
40 消しゴム    50
85 コンパス   230

Z:\clang\kadai10>type syohin2.mst
37 定規       150
63 ボールペン  80
74 分度器     370

Z:\clang\kadai10>kadai1006
C言語実習課題10-6 マージ
入力ファイル(商品マスタ１)：　4 レコード
入力ファイル(商品マスタ２)：　3 レコード
出力ファイル(商品マスタ３)：　7 レコード

Z:\clang\kadai10>type syohin3.mst
16 鉛筆        30
21 ノート     100
37 定規       150
40 消しゴム    50
63 ボールペン  80
74 分度器     370
85 コンパス   230
*/