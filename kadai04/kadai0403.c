#include <stdio.h>

int main(void){
	int no,end_flag;
	double shincho,taiju,bmi;
	printf("C言語実習課題 4-3 BMI\nNO 身長 体重  BMI\n-- ---- ---- -----\n");
	end_flag=scanf("%d%lf%lf",&no,&shincho,&taiju);
	while(end_flag!=EOF){
		bmi=taiju/(shincho*shincho);
		printf(" %d %.2f %.1f %.2f\n",no,shincho,taiju,bmi);
		end_flag=scanf("%d%lf%lf",&no,&shincho,&taiju);
	}
	return 0;
}
/*
Z:\clang\kadai04>kadai0403<bmi2.txt
C言語実習課題 4-3 BMI
NO 身長 体重  BMI
-- ---- ---- -----
 1 1.72 65.8 22.24
 2 1.80 55.5 17.13
 3 1.51 46.2 20.26
 4 1.65 75.1 27.58
 5 1.69 60.7 21.25
*/