#include <stdio.h>

int main(void){
	printf("C言語実習課題 4-6 三角形判定\n");
	int a,b,c,wk,end_flag;
	end_flag=scanf("%d%d%d",&a,&b,&c);
	while(end_flag!=EOF){
		printf("%d %d %d　",a,b,c);
		if(a>=b&&a>=c){
			wk=c;
			c=a;
			a=wk;
		}else if(b>=c&&b>=a){
			wk=c;
			c=b;
			b=wk;
		}
		if(c<a+b){
			if(a==b&&b==c){
				printf("正三角形です。\n");
			}else if(c*c==a*a+b*b){
				printf("直角三角形です。\n");
			}else if(c*c<a*a+b*b){
				if(a==b||b==c||c==a){
					printf("鋭角三角形で二等辺三角形です。\n");
				}else{
					printf("鋭角三角形です。\n");
				}
			}else{
				if(a==b||b==c||c==a){
					printf("鈍角三角形で二等辺三角形です。\n");
				}else{
					printf("鈍角三角形です。\n");
				}
			}
		}else{
			printf("三角形は成立しません。\n");
		}
		end_flag=scanf("%d%d%d",&a,&b,&c);
	}
	return 0;
}

/*
	Z:\clang\kadai04>kadai0406<sankaku.txt
	C言語実習課題 4-6 三角形判定
	30 30 30　正三角形です。
	40 50 30　直角三角形です。
	60 50 40　鋭角三角形です。
	40 20 30　鈍角三角形です。
	30 10 30　鋭角三角形で二等辺三角形です。
	20 30 20　鈍角三角形で二等辺三角形です。
	30 60 20　三角形は成立しません。

*/