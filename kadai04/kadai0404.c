#include <stdio.h>

int main(void){
	int no,point,max=1,min=100,sum=0,pass=0,end_flag;
	double avg;
	printf("C言語実習課題 4-4 成績一覧\n得点\n--------------------\n");
	end_flag=scanf("%d%d",&no,&point);
	while(end_flag!=EOF){
		printf("%4d",point);
		if(point>=60){
			pass++;
		}
		if(no%5==0){
			printf("\n");
		}
		if(point>max){
			max=point;
		}
		if(point<min){
			min=point;
		}
		sum+=point;
		end_flag=scanf("%d%d",&no,&point);
	}
	avg=(double)sum/no;
	if(no%5!=0){
		printf("\n");
	}
		printf("--------------------\n",no);
		printf("受験者数　%4d\n",no);
		printf("合格者数　%4d\n",pass);
		printf("不合格者数%4d\n",no-pass);
		printf("最高点　　%4d\n",max);
		printf("最低点　　%4d\n",min);
		printf("平均点　　%3.1f\n",avg);
	return 0;
}
/*
	Z:\clang\kadai04>kadai0404<seiseki1.txt
	C言語実習課題 4-4 成績一覧
	得点
	--------------------
	  82 100  63  56  97
	  76  51  85 100  94
	  57  91
	--------------------
	受験者数　  12
	合格者数　   9
	不合格者数   3
	最高点　　 100
	最低点　　  51
	平均点　　79.3

	Z:\clang\kadai04>kadai0404<seiseki2.txt
	C言語実習課題 4-4 成績一覧
	得点
	--------------------
	  75  60  70  91  83
	  55  63  77  90  50
	  64  58  54  73  68
	--------------------
	受験者数　  15
	合格者数　  11
	不合格者数   4
	最高点　　  91
	最低点　　  50
	平均点　　68.7


*/