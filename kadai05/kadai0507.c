#include <stdio.h>

int main(void)
{
	int end_flag,i=0,j,k,m,temp, array[10];
	printf("C言語実習課題5-7 バブルソート\n");
	printf("*** ソート前 ***\n");
	end_flag=scanf("%d",&array[i]);
	while(end_flag!=EOF){
		printf("%4d",array[i]);
		i++;
		end_flag=scanf("%d",&array[i]);
	}
	printf("\n");
	for(j=0;j<i-1;j++){
		for(k=i-1;k>j;k=k-1){
			if(array[k]<array[k-1]){
				temp=array[k];
				array[k]=array[k-1];
				array[k-1]=temp;
			}
		}
	}
	printf("*** ソート後 ***\n");
	for(m=0;m<i;m++){
		printf("%4d",array[m]);
	}
	printf("\n");
	return 0;
}

/*
Z:\clang\kadai05>kadai0507<data1.txt
C言語実習課題5-7 バブルソート
*** ソート前 ***
  50  22  65  10  90  33  45  51  80  74
*** ソート後 ***
  10  22  33  45  50  51  65  74  80  90

Z:\clang\kadai05>kadai0507<data2.txt
C言語実習課題5-7 バブルソート
*** ソート前 ***
 238 621 447 129 315
*** ソート後 ***
 129 238 315 447 621

Z:\clang\kadai05>kadai0507<data3.txt
C言語実習課題5-7 バブルソート
*** ソート前 ***
 813
*** ソート後 ***
 813
*/