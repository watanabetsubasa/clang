#include <stdio.h>

int main(void){
	printf("C言語実習課題5-4　年月日チェック\n");
	int end_of_month[12]={31,28,31,30,31,30,31,31,30,31,30,31},year,month,day;
	printf("年 ==> ");
	scanf("%d",&year);
	printf("月 ==> ");
	scanf("%d",&month);
	printf("日 ==> ");
	scanf("%d",&day);
	if(1900<=year&&year<=2100){
		if(1<=month&&month<=12){
			if((1<=day&&day<=end_of_month[month-1])||(month==2&&(year%400==0||(year%4==0&&year%100!=0))&&(1<=day&&day<=29))){
				printf("%d年%d月%d日は、正しい日付です。",year,month,day);
			}else{
				printf("%d年%d月%d日は、誤った日付です。",year,month,day);
			}
		}else{
			printf("%d年%d月%d日は、誤った日付です。",year,month,day);
		}
	}else{
		printf("%d年%d月%d日は、誤った日付です。",year,month,day);
	}
	return 0;
}
/*
Z:\clang\kadai05>kadai0504
Z:\clang\kadai05>kadai0504
C言語実習課題5-4　年月日チェック
年 ==> 2024
月 ==> 2
日 ==> 29
2024年2月29日は、正しい日付です。
Z:\clang\kadai05>kadai0504
C言語実習課題5-4　年月日チェック
年 ==> 2023
月 ==> 2
日 ==> 29
2023年2月29日は、誤った日付です。
Z:\clang\kadai05>kadai0504
C言語実習課題5-4　年月日チェック
年 ==> 1899
月 ==> 12
日 ==> 31
1899年12月31日は、誤った日付です。
Z:\clang\kadai05>kadai0504
C言語実習課題5-4　年月日チェック
年 ==> 2022
月 ==> 13
日 ==> 13
2022年13月13日は、誤った日付です。
1<=
Z:\clang\kadai05>kadai0504

&&(1<=day&&day<=29)の確認
C言語実習課題5-4　年月日チェック
年 ==> 2024
月 ==> 2
日 ==> 30
2024年2月30日は、誤った日付です。
C言語実習課題5-4　年月日チェック
年 ==> 2024
月 ==> 2
日 ==> 0
2024年2月0日は、誤った日付です。
*/