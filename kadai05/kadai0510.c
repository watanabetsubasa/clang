#include <stdio.h>

int main(void)
{
	int size,i,j,max,count,sum_naname1=0,sum_naname2=0;
	printf("C言語実習課題10 魔法陣\n");
	printf("魔法陣のサイズ ==> ");
	scanf("%d",&size);
	while(!(3<=size&&size<=11)&&(size%2==1)){
		printf("サイズは3以上11以下の奇数です。\n");
		printf("魔法陣のサイズ ==> ");
		scanf("%d",&size);
	}
	int mahojin[11][11]={0};
	mahojin[(size-1)/2][0]=1;
	i=(size-1)/2;
	j=0;
	max=size*size;
	count=1;
	do{
		//位置特定
		if(i==0){
			i=size-1;
		}else{
			i--;
		}
		if(j==0){
			j=size-1;
		}else{
			j--;
		}
		//求めた位置が未設定の時、数字を埋める
		if(mahojin[i][j]==0){
		}else{
		//設定済みの場合は最後に埋めた位置に戻る
			if(i==size-1){
				i=0;
			}else{
				i=i+1;
			}
			if(j==size-1){
				j=0;
			}else{
				j=j+1;
			}
		//最後の位置から右に埋める
			if(j==size-1){
				j=0;
			}else{
				j++;
			}
		}
		count++;
		mahojin[i][j]=count;
	}while(count<max);
	//右斜め上を求めて出力
	i=0;
	j=size-1;
	while((i<size)&&(j>=0)){
		sum_naname1=sum_naname1+mahojin[i][j];
		i++;
		j--;
	}
	printf("%30d\n",sum_naname1);
	//列の値と合計を出力
	int sum_yoko[11]={0};
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			printf("%5d",mahojin[i][j]);
			sum_yoko[i]+=mahojin[i][j];
		}
		printf("%5d\n",sum_yoko[i]);
	}
	printf("\n");
	//行の合計を出力
	int sum_tate[11]={0};
	for(j=0;j<size;j++){
		for(i=0;i<size;i++){
			sum_tate[j]+=mahojin[i][j];
		}
		printf("%5d",sum_tate[j]);
	}
	//斜め下合計を求める
	i=0;
	j=0;
	while((j<size)&&(i<size)){
		sum_naname2+=mahojin[i][j];
		j++;
		i++;
	}
	printf("%5d\n",sum_naname2);
	return 0;
}
