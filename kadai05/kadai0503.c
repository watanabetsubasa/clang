#include <stdio.h>

int main(void){
	printf("C言語実習課題5-3　二次元配列の行列変換\n");
	int array1[3][5]={{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15}},array2[5][3],i,j;
	printf("*** 交換前 array1[3][5] ***\n");
	for(i=0;i<3;i++){
		for(j=0;j<5;j++){
			printf("%3d",array1[i][j]);
		}
			printf("\n");
	}
	//変換
	for(i=0;i<3;i++){
		for(j=0;j<5;j++){
			array2[j][i]=array1[i][j];
		}
	}
	printf("*** 交換後 array2[5][3] ***\n");
	for(i=0;i<5;i++){
		for(j=0;j<3;j++){
			printf("%3d",array2[i][j]);
		}
			printf("\n");
	}
	return 0;
}
/*
Z:\clang\kadai05>kadai0503
C言語実習課題5-3　二次元配列の行列変換
*** 交換前 array1[3][5] ***
  1  2  3  4  5
  6  7  8  9 10
 11 12 13 14 15
*** 交換後 array2[5][3] ***
  1  6 11
  2  7 12
  3  8 13
  4  9 14
  5 10 15
*/