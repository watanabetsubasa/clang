#include <stdio.h>

int main(void)
{
	int i,j,k;
	double array[5]={160.7,162.8,155.5,168.3,160.1},temp;
	printf("C言語実習課題 5-5 配列\n");
	printf("*** ソート前 ***\n");
	for(k=0;k<5;k++){
		printf("  %4.1f",array[k]);
	}
	printf("\n");
	for(i=0;i<4;i++){
		for(j=i+1;j<5;j++){
			if(array[i]<array[j]){
				temp=array[i];
				array[i]=array[j];
				array[j]=temp;
			}
		}
	}
	printf("*** ソート後 ***\n");
	for(k=0;k<5;k++){
		printf("  %4.1f",array[k]);
	}
	printf("\n");
	return 0;
}
/*
Z:\clang\kadai05>kadai0505
C言語実習課題 5-5 配列
*** ソート前 ***
  160.7  162.8  155.5  168.3  160.1
*** ソート後 ***
  168.3  162.8  160.7  160.1  155.5
*/