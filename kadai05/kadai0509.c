#include <stdio.h>

int main(void)
{
	double data[5]={160.7,162.8,155.5,168.3,160.1}, temp;
	int i,j,max_ord;
	printf("C言語実習課題5-9 基本選択法\n*** ソート前 ***\n");
	for(i=0;i<5;i++){
		printf("%7.1f",data[i]);
	}
	printf("\n");
	for(i=0;i<=3;i++){
		max_ord=i;
		for(j=i+1;j<=4;j++){
			if(data[max_ord]<data[j]){
				max_ord=j;
			}
		}
		temp=data[i];
		data[i]=data[max_ord];
		data[max_ord]=temp;
	}
	printf("*** ソート後 ***\n");
	for(i=0;i<5;i++){
		printf("%7.1f",data[i]);
	}
	printf("\n");
	return 0;
}
/*
Z:\clang\kadai05>kadai0509
C言語実習課題5-9 基本選択法
*** ソート前 ***
  160.7  162.8  155.5  168.3  160.1
*** ソート後 ***
  168.3  162.8  160.7  160.1  155.5
*/