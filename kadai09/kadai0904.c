#include <stdio.h>

struct product{
	int no;
	char name[11];
	int price;
};

int main(void)
{
	int in_code;
	struct product data[]={{21,"鉛筆",30},{68,"ノート",100},{37,"定規",150},{40,"消しゴム",50},{85,"コンパス",230}},*pi;
	
	printf("C言語実習課題9-4 商品検索\n");
	
	printf("商品コード ==> ");
	scanf("%d",&in_code);
	
	while(in_code!=99){
		for(pi=data;pi<data+5&&in_code!=pi->no;pi++);
		if(pi<data+5){
			printf("商品名 : %s\n単価 : %d\n",pi->name,pi->price);
		}else{
			printf("該当する商品はありません。\n");
		}
	
		printf("商品コード ==> ");
		scanf("%d",&in_code);
	}
	
	return 0;
	
}
/*
Z:\clang\kadai09>kadai0904
C言語実習課題9-4 商品検索
商品コード ==> 37
商品名 : 定規
単価 : 150
商品コード ==> 12
該当する商品はありません。
商品コード ==> 85
商品名 : コンパス
単価 : 230
商品コード ==> 99

Z:\clang\kadai09>kadai0904
C言語実習課題9-4 商品検索
商品コード ==> 99
*/