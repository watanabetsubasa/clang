#include <stdio.h>

struct seiseki{
	int no;
	char name[11];
	int eigo
	int sugaku
	int kokugo
};

int main(void)
{
	int i,j,end_flag,kensu,eigo_rank[5],sugaku_rank[5],kokugo_rank[5],temp_rank;
	struct seiseki input,data[5],temp;
	
	printf("C言語実習課題9-6 科目別成績一覧表\n");
	
	end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.eigo,&input.sugaku,&input.kokugo);
	
	for(i=0;end_flag!=EOF;i++){
		data[i]=input;
		end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.eigo,&input.sugaku,&input.kokugo);
	}
	
	kensu=i;
	
	//順位算出
	
	for(i=0;i<kensu;i++){
		for(j=0;j<kensu;j++){
			if(data[i].eigo<data[j].eigo){
				eigo_rank[i]++;
			}
			
			if(data[i].sugaku<data[j].sugaku){
				sugaku_rank[]++;
			}
			
			if(data[i].score.kokugo<data[j].score.kokugo){
				kokugo_rank[i]++;
			}
		}
	}
	
	//出力順ソート・出力
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(eigo_rank[i]>eigo_rank[j]){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
				temp_rank=data[i];
				data[i]=data[j];
				data[j]=temp_rank;
			}else if(eigo_rank[i]==eigo_rank[j]){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
					temp_rank=data[i];
					data[i]=data[j];
					data[j]=temp_rank;
				}
			}
			if(eigo_rank[i]>eigo_rank[j]){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
				temp_rank=data[i];
				data[i]=data[j];
				data[j]=temp_rank;
			}else if(eigo_rank[i]==eigo_rank[j]){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
					temp_rank=data[i];
					data[i]=data[j];
					data[j]=temp_rank;
				}
			}
			if(eigo_rank[i]>eigo_rank[j]){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
				temp_rank=data[i];
				data[i]=data[j];
				data[j]=temp_rank;
			}else if(eigo_rank[i]==eigo_rank[j]){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
					temp_rank=data[i];
					data[i]=data[j];
					data[j]=temp_rank;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (英語) ***\n NO 氏　　名　 英語 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].eigo,data[i].eigo_rank[i]);
	}
	printf("\n");
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(data[i].rank.sugaku>data[j].rank.sugaku){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
			}else if(data[i].rank.sugaku==data[j].rank.sugaku){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (数学) ***\n NO 氏　　名　 数学 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].score.sugaku,data[i].rank.sugaku);
	}
	printf("\n");
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(data[i].rank.kokugo>data[j].rank.kokugo){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
			}else if(data[i].rank.kokugo==data[j].rank.kokugo){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (国語) ***\n NO 氏　　名　 国語 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].score.kokugo,data[i].rank.kokugo);
	}
	printf("\n");
	
	return 0;
}
/*

*/
