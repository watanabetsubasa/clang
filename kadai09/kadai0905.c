#include <stdio.h>

struct product{
	int code;
	char name[11];
	int tanka;
};

int main(void)
{
	struct product data[]={
		{21,"鉛筆",30},
		{68,"ノート",100},
		{37,"定規",150},
		{40,"消しゴム",50},
		{85,"コンパス",230},
	};
	int in_code,in_soldQty,end_flag,i,sum=0;
	
	printf("C言語実習課題9-5 売上一覧表\n\n　　　 *** 売上一覧表 ***\nコード 商　品　名 単価 数量  金額\n------ ---------- ---- ---- -----\n");
	
	end_flag=scanf("%d%d",&in_code,&in_soldQty);
	
	while(end_flag!=EOF){
		for(i=0;in_code!=data[i].code;i++){
		}
		
		printf("%6d %-10s %4d %4d %5d\n",data[i].code,data[i].name,data[i].tanka,in_soldQty,data[i].tanka*in_soldQty);
		
		sum=sum+data[i].tanka*in_soldQty;
		
		end_flag=scanf("%d%d",&in_code,&in_soldQty);
	}
	
	printf("---------------------------------\n　　　　　　　　　合計金額　%5d",sum);
	
	return 0;
}
/*
Z:\clang\kadai09>kadai0905<uriage.txt
C言語実習課題9-5 売上一覧表

　　　 *** 売上一覧表 ***
コード 商　品　名 単価 数量  金額
------ ---------- ---- ---- -----
    68 ノート      100   30  3000
    85 コンパス    230   11  2530
    21 鉛筆         30  120  3600
    40 消しゴム     50   22  1100
    37 定規        150   18  2700
---------------------------------
　　　　　　　　　合計金額　12930
*/