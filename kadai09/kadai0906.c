#include <stdio.h>

struct subject{
	int eigo;
	int sugaku;
	int kokugo;
};

struct seiseki{
	int no;
	char name[11];
	struct subject score;
	struct subject rank;
};

int main(void)
{
	int i,j,end_flag,kensu;
	struct seiseki input,data[5],temp;
	
	printf("C言語実習課題9-6 科目別成績一覧表\n");
	
	end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.score.eigo,&input.score.sugaku,&input.score.kokugo);
	
	input.rank.eigo=1;
	input.rank.sugaku=1;
	input.rank.kokugo=1;
	
	for(i=0;end_flag!=EOF;i++){
		data[i]=input;
		end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.score.eigo,&input.score.sugaku,&input.score.kokugo);
	}
	
	kensu=i;
	
	//順位算出
	
	for(i=0;i<kensu;i++){
		for(j=0;j<kensu;j++){
			if(data[i].score.eigo<data[j].score.eigo){
				data[i].rank.eigo++;
			}
			
			if(data[i].score.sugaku<data[j].score.sugaku){
				data[i].rank.sugaku++;
			}
			
			if(data[i].score.kokugo<data[j].score.kokugo){
				data[i].rank.kokugo++;
			}
		}
	}
	
	//出力順ソート・出力
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(data[i].rank.eigo>data[j].rank.eigo){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
			}else if(data[i].rank.eigo==data[j].rank.eigo){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (英語) ***\n NO 氏　　名　 英語 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].score.eigo,data[i].rank.eigo);
	}
	printf("\n");
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(data[i].rank.sugaku>data[j].rank.sugaku){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
			}else if(data[i].rank.sugaku==data[j].rank.sugaku){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (数学) ***\n NO 氏　　名　 数学 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].score.sugaku,data[i].rank.sugaku);
	}
	printf("\n");
	
	for(i=0;i<kensu-1;i++){
		for(j=i+1;j<kensu;j++){
			if(data[i].rank.kokugo>data[j].rank.kokugo){
				temp=data[i];
				data[i]=data[j];
				data[j]=temp;
			}else if(data[i].rank.kokugo==data[j].rank.kokugo){
				if(data[i].no>data[j].no){
					temp=data[i];
					data[i]=data[j];
					data[j]=temp;
				}
			}
		}
	}
	
	printf("*** 成績一覧表 (国語) ***\n NO 氏　　名　 国語 順位\n--- ---------- ---- ----\n");
	for(i=0;i<kensu;i++){
		printf("%3d %10s %4d %4d\n",data[i].no,data[i].name,data[i].score.kokugo,data[i].rank.kokugo);
	}
	printf("\n");
	
	return 0;
}
/*
Z:\clang\kadai09>kadai0906<seiseki.txt
C言語実習課題9-6 科目別成績一覧表
*** 成績一覧表 (英語) ***
 NO 氏　　名　 英語 順位
--- ---------- ---- ----
147   Williams   81    1
206      Brown   81    1
553    Johnson   70    3
447      Jones   65    4
312      Smith   60    5

*** 成績一覧表 (数学) ***
 NO 氏　　名　 数学 順位
--- ---------- ---- ----
553    Johnson   92    1
447      Jones   88    2
312      Smith   82    3
147   Williams   66    4
206      Brown   66    4

*** 成績一覧表 (国語) ***
 NO 氏　　名　 国語 順位
--- ---------- ---- ----
447      Jones   90    1
147   Williams   74    2
312      Smith   74    2
553    Johnson   74    2
206      Brown   70    5
*/
