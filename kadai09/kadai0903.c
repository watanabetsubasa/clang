#include <stdio.h>

struct seiseki{
	int no;
	char name[11];
	int eigo;
	int sugaku;
	int kokugo;
};

int main(void)
{
	int i,j,end_flag,kensu,sum[10],rank[10];
	double avg[10];
	struct seiseki input,data[10];
	
	printf("C言語実習課題9-3 成績一覧表\n");
	
	end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.eigo,&input.sugaku,&input.kokugo);
	for(i=0;end_flag!=EOF;i++){
		data[i]=input;
		end_flag=scanf("%d%s%d%d%d",&input.no,input.name,&input.eigo,&input.sugaku,&input.kokugo);
	}
	
	kensu=i;
	
	for(i=0;i<kensu;i++){
		sum[i]=data[i].eigo+data[i].sugaku+data[i].kokugo;
		avg[i]=(double)sum[i]/3;
	}
	
	for(i=0;i<kensu;i++){
		rank[i]=1;
		for(j=0;j<kensu;j++){
			if(sum[i]<sum[j]){
				rank[i]=rank[i]+1;
			}
		}
	}
	
	printf(" NO 氏　　名　 英語 数学 国語 合計  平均 順位\n--- ---------- ---- ---- ---- ---- ----- ----\n");
	
	for(i=0;i<5;i++){
		printf("%3d %-10s %4d %4d %4d %4d %5.1f %4d\n",data[i].no,data[i].name,data[i].eigo,data[i].sugaku,data[i].kokugo,sum[i],avg[i],rank[i]);
	}
	
	return 0;
}
/*
Z:\clang\kadai09>kadai0903<seiseki.txt
C言語実習課題9-3 成績一覧表
 NO 氏　　名　 英語 数学 国語 合計  平均 順位
--- ---------- ---- ---- ---- ---- ----- ----
312 Smith        60   82   74  216  72.0    5
553 Johnson      70   92   74  236  78.7    2
147 Williams     81   66   74  221  73.7    3
206 Brown        81   66   70  217  72.3    4
447 Jones        65   88   90  243  81.0    1
*/