#include <stdio.h>

struct ymd{
	int yy;
	int mm;
	int dd;
};

struct employees{
	int no;
	char name[11];
	struct ymd hireDate;
	struct ymd birthDate;
};

int main(void)
{
	int i,j,hikaku1,hikaku2;
	struct employees shain[5]={
		{6447,"Wilson",1998,3,25,1985,4,20},
		{5206,"Rodriguez",1995,4,20,1986,10,10},
		{3147,"Garicia",2001,10,1,1986,10,11},
		{1212,"Miller",2012,4,1,1992,6,23},
		{2353,"Davis",2011,4,1,1994,9,30},
	},temp;
	
	printf("C言語実習課題9-2 社員名簿\n");
	printf("社員番号 氏　　名　 入社年月日　生年月日\n-------- ---------- ---------- ----------\n");
	
	for(i=0;i<4;i++){
		for(j=i+1;j<5;j++){
			hikaku1=10000*shain[i].birthDate.yy+100*shain[i].birthDate.mm+shain[i].birthDate.dd;
			hikaku2=10000*shain[j].birthDate.yy+100*shain[j].birthDate.mm+shain[j].birthDate.dd;
			
			if(hikaku1>hikaku2){
				temp=shain[i];
				shain[i]=shain[j];
				shain[j]=temp;
			}
		}
	}
	
	for(i=0;i<5;i++){
		printf("%8d %-10s %d/%2d/%2d %d/%2d/%2d\n",shain[i].no,shain[i].name,shain[i].hireDate.yy,shain[i].hireDate.mm,shain[i].hireDate.dd,shain[i].birthDate.yy,shain[i].birthDate.mm,shain[i].birthDate.dd);
	}
	return 0;
}
/*
Z:\clang\kadai09>kadai0902
C言語実習課題9-2 社員名簿
社員番号 氏　　名　 入社年月日　生年月日
-------- ---------- ---------- ----------
    6447 Wilson     1998/ 3/25 1985/ 4/20
    5206 Rodriguez  1995/ 4/20 1986/10/10
    3147 Garicia    2001/10/ 1 1986/10/11
    1212 Miller     2012/ 4/ 1 1992/ 6/23
    2353 Davis      2011/ 4/ 1 1994/ 9/30
*/