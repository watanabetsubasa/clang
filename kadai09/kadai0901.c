#include <stdio.h>

struct person{
	int no;
	char name[11];
	double height;
	double weight;
};

int main(void)
{
	struct person person1,person2;
	
	printf("C言語実習課題9-1 構造体の入出力とコピー\n");
	
	printf("コピー元構造体の入力\n");
	printf("番号 ==> ");
	scanf("%d",&person1.no);
	printf("氏名 ==> ");
	scanf("%s",person1.name);
	printf("身長 ==> ");
	scanf("%lf",&person1.height);
	printf("体重 ==> ");
	scanf("%lf",&person1.weight);
	
	person2=person1;
	
	printf("\nコピー元構造体\n");
	printf("番号 : %d\n氏名 : %s\n身長 : %.1f\n体重 : %.1f\n",person1.no,person1.name,person1.height,person1.weight);
	
	printf("\nコピー先構造体\n");
	printf("番号 : %d\n氏名 : %s\n身長 : %.1f\n体重 : %.1f\n",person2.no,person2.name,person2.height,person2.weight);
	return 0;
}
/*
Z:\clang\kadai09>kadai0901
C言語実習課題9-1 構造体の入出力とコピー
コピー元構造体の入力
番号 ==> 57
氏名 ==> 東京太郎
身長 ==> 173.4
体重 ==> 56.7

コピー元構造体
番号 : 57
氏名 : 東京太郎
身長 : 173.4
体重 : 56.7

コピー先構造体
番号 : 57
氏名 : 東京太郎
身長 : 173.4
体重 : 56.7
*/