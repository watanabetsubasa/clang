#include <stdio.h>
#include <string.h>
void search(char *eng,char **pword);

int main(void)
{
	char eng[100],*word[]={"apple","林檎","banana","バナナ","grape","葡萄","pear","梨","strawberry","苺",NULL};
	int result;
	
	printf("C言語実習課題8-17 英和辞典\n");
	
	printf("英語 ==> ");
	scanf("%s",eng);
	
	result=strcmp(eng,"end");
	
	while(result!=0){
		search(eng,word);
		
		printf("英語 ==> ");
		scanf("%s",eng);
		
		result=strcmp(eng,"end");
	}
	
	return 0;
}

void search(char *eng,char **pword)
{
	char **pi=pword,**pja;
	int result=1,resultNull=1;
	
	while(result!=0&&resultNull!=0){
		if(*pi==NULL){
			resultNull=0;
		}
		
		if(resultNull!=0){
			result=strcmp(eng,*pi);
			
			if(result==0){
				pja=pi+1;
				printf("日本語 = %s\n",*pja);
			}
			pi=pi+2;
		}else{
			printf("該当する英単語はありません。\n");
		}
	}
}





/*
Z:\clang\kadai08>kadai0817
C言語実習課題8-17 英和辞典
英語 ==> pear
日本語 = 梨
英語 ==> banana
日本語 = バナナ
英語 ==> pineapple
該当する英単語はありません。
英語 ==> apple
日本語 = 林檎
英語 ==> pear
日本語 = 梨
英語 ==> strawberry
日本語 = 苺
英語 ==> grape
日本語 = 葡萄
英語 ==> end

Z:\clang\kadai08>kadai0817
C言語実習課題8-17 英和辞典
英語 ==> end

Z:\clang\kadai08>kadai0817
C言語実習課題8-17 英和辞典
英語 ==> pineapple
該当する英単語はありません。
英語 ==> NULL
該当する英単語はありません。
英語 ==> END
該当する英単語はありません。
英語 ==> end
*/