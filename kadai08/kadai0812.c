#include <stdio.h>
int str_comp(char *p1,char *p2);

int main(void)
{
	printf("C言語実習課題8-12 文字列の比較\n");
	
	char str1[256],str2[256];
	int result;
	
	printf("１つの文字列 ==> ");
	scanf("%s",str1);
	printf("２つの文字列 ==> ");
	scanf("%s",str2);
	
	result=str_comp(str1,str2);
	
	if(result>0){
		printf("%s > %s\n",str1,str2);
	}else if(result==0){
		printf("%s = %s\n",str1,str2);
	}else{
		printf("%s < %s\n",str1,str2);
	}
	
	return 0;
}

int str_comp(char *p1,char *p2)
{
	int result;
	
	for(;*p1==*p2&&*p1!='\0';p1++,p2++){
	}
	
	if(*p1>*p2){
		result=1;
	}else if(*p1==*p2){
		result=0;
	}else{
		result=-1;
	}
	
	return result;
}

/*
Z:\clang\kadai08>kadai0812
C言語実習課題8-12 文字列の比較
１つの文字列 ==> abc
２つの文字列 ==> abbc
abc > abbc

Z:\clang\kadai08>kadai0812
C言語実習課題8-12 文字列の比較
１つの文字列 ==> abcd
２つの文字列 ==> abd
abcd < abd

Z:\clang\kadai08>kadai0812
C言語実習課題8-12 文字列の比較
１つの文字列 ==> abc
２つの文字列 ==> abc
abc = abc
*/