#include <stdio.h>
void str_copy(char *pstr1,char *pstr2);

int main(void)
{
	printf("C言語実習課題8-10 文字列のコピー\n");
	
	char str1[256],str2[256];
		
	printf("コピー元文字列 ==> ");
	scanf("%s",str2);
	
	str_copy(str1,str2);
	
	printf("コピー元文字列　：　%s\n",str2);
	printf("コピー先文字列　：　%s\n",str1);
	
	return 0;
}

void str_copy(char *pstr1,char *pstr2)
{
	char *pi,*pj;
	
	for(pi=pstr1,pj=pstr2;*pj!='\0';pi++,pj++){
		*pi=*pj;
	}
	*pi='\0';
}

/*
Z:\clang\kadai08>kadai0810
C言語実習課題8-10 文字列のコピー
コピー元文字列 ==> abcdefg
コピー元文字列　：　abcdefg
コピー先文字列　：　abcdefg

Z:\clang\kadai08>kadai0810
C言語実習課題8-10 文字列のコピー
コピー元文字列 ==> 123XYZ#$%
コピー元文字列　：　123XYZ#$%
コピー先文字列　：　123XYZ#$%
*/
