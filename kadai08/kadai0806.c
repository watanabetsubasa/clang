#include <stdio.h>

int main(void)
{
	printf("C言語実習課題8-6 パスワード文字列構成検査\n");
	
	char pass[256];
	int i,figure_count=0,upper_count=0,lower_count=0;
	
	printf("パスワード ==> ");
	scanf("%s",pass);
	
	for(i=0;pass[i];i++){
		if('0'<=pass[i]&&pass[i]<='9'){
			figure_count++;
		}else if('A'<=pass[i]&&pass[i]<='Z'){
			upper_count++;
		}else if('a'<=pass[i]&&pass[i]<='z'){
			lower_count++;
		}
	}
	
	if(6<=i&&i<=10&&figure_count>=1&&upper_count>=1&&lower_count>=1){
		printf("正しいパスワード文字列です。\n");
	}else{
		if(i<6||10<i){
			printf("エラー：文字列は6文字以上10文字以上以下です。\n");
		}
		if(figure_count==0){
			printf("エラー：数字(0-9)は１文字以上必要です。\n");
		}
		if(upper_count==0){
			printf("エラー：アルファベット大文字は１文字以上必要です。\n");
		}
		if(lower_count==0){
			printf("エラー：アルファベット小文字は１文字以上必要です。\n");
		}
	}
	
	return 0;
}
/*
Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> 123ABCxyz
正しいパスワード文字列です。

Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> abc123XYZ567
エラー：文字列は6文字以上10文字以上以下です。

Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> ABCDefgh
エラー：数字(0-9)は１文字以上必要です。

Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> 1234xyz
エラー：アルファベット大文字は１文字以上必要です。

Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> 1234XYZ
エラー：アルファベット小文字は１文字以上必要です。

Z:\clang\kadai08>kadai0806
C言語実習課題8-6 パスワード文字列構成検査
パスワード ==> #$%&
エラー：文字列は6文字以上10文字以上以下です。
エラー：数字(0-9)は１文字以上必要です。
エラー：アルファベット大文字は１文字以上必要です。
エラー：アルファベット小文字は１文字以上必要です。

*/