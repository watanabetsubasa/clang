#include <stdio.h>

int main(void)
{
	printf("C言語実習課題8-2 文字列の連結\n");
	
	char str1[256],str2[256];
	int i,j;
	
	printf("１つ目の文字列 ==> ");
	scanf("%s",str1);
	printf("２つ目の文字列 ==> ");
	scanf("%s",str2);
	
	for(i=0;str1[i];i++){
	}
	for(j=0;str2[j];j++){
	}
	
	if(i+j<=20){
		for(j=0;str2[j];i++,j++){
			str1[i]=str2[j];
		}
		str1[i]='\0';
		printf("連結後の１つ目の文字列 : %s\n",str1);
	}else{
		printf("合計で20文字を超えたので連結しません。\n");
	}
	
	return 0;
}

/*
Z:\clang\kadai08>kadai0802
C言語実習課題8-2 文字列の連結
１つ目の文字列 ==> abcdefg
２つ目の文字列 ==> hijklmn
連結後の１つ目の文字列 : abcdefghijklmn

Z:\clang\kadai08>kadai0802
C言語実習課題8-2 文字列の連結
１つ目の文字列 ==> abcdefghi
２つ目の文字列 ==> jklmnopqrst
連結後の１つ目の文字列 : abcdefghijklmnopqrst

Z:\clang\kadai08>kadai0802
C言語実習課題8-2 文字列の連結
１つ目の文字列 ==> abcdefghijklmn
２つ目の文字列 ==> opqrstuvwxyz
合計で20文字を超えたので連結しません。

*/
