#include <stdio.h>
#include <string.h>

int main(void)
{
	printf("C言語実習課題8-1 文字列のコピー\n");
	
	char str1[256],str2[256];
	int i;
	
	printf("コピー元文字列 ==> ");
	scanf("%s",str2);
	
	strcpy(str1,str2);
	
	printf("コピー元文字列　：　%s\n",str2);
	printf("コピー先文字列　：　%s\n",str1);
	
	return 0;
}

/*
Z:\clang\kadai08>kadai0807
C言語実習課題8-1 文字列のコピー
コピー元文字列 ==> abcdefg
コピー先文字列　：　abcdefg
コピー先文字列　：　abcdefg

Z:\clang\kadai08>kadai0807
C言語実習課題8-1 文字列のコピー
コピー元文字列 ==> 123XYZ#$%
コピー先文字列　：　123XYZ#$%
コピー先文字列　：　123XYZ#$%
*/
