#include <stdio.h>

int main(void)
{
	printf("C言語実習課題8-5 数字チェック\n");
	
	char str[256];
	int i,count=0;
	
	printf("文字列 ==> ");
	scanf("%s",str);
	
	for(i=0;str[i];i++){
		if(str[i]<'0'||'9'<str[i]){
			count++;
		}
	}
	
	if(count==0){
		printf("すべて数字で構成されています。\n");
	}else{
		printf("数字以外の文字が%d文字混在しています。\n",count);
	}
	
	return 0;
}

/*
Z:\clang\kadai08>kadai0805
C言語実習課題8-5 数字チェック
文字列 ==> 512872943
すべて数字で構成されています。

Z:\clang\kadai08>kadai0805
C言語実習課題8-5 数字チェック
文字列 ==> 85A6921B75503
数字以外の文字が2文字混在しています。

Z:\clang\kadai08>kadai0805
C言語実習課題8-5 数字チェック
文字列 ==> 19$43#AB93=2
数字以外の文字が5文字混在しています。
*/