#include <stdio.h>
void search(int in_code,int *pcode,char **pname,int *pprice,int size);

int main(void)
{
	int code[]={21,68,37,40,85},price[]={30,100,150,50,230},in_code;
	char *name[]={"鉛筆","ノート","定規","消しゴム","コンパス"};
	
	printf("C言語実習課題8-13 商品検索\n");
	
	printf("商品コード ==> ");
	scanf("%d",&in_code);
	
	while(in_code!=99){
		search(in_code,code,name,price,5);
		
		printf("商品コード ==> ");
		scanf("%d",&in_code);
	}
	
	return 0;
}

void search(int in_code,int *pcode,char **pname,int *pprice, int size)
{
	int *pi,count=0;
	
	for(pi=pcode;pi<pcode+size&&in_code!=*pi;count++,pi++){
	}
	
	if(pi<pcode+size){
		printf("商品名 : %s\n",*(pname+count));
		printf("単価 : %d\n",*(pprice+count));
	}else{
		printf("該当する商品はありません。\n");
	}
}
/*
Z:\clang\kadai08>kadai0813
C言語実習課題8-13 商品検索
商品コード ==> 37
商品名 : 定規
単価 : 150
商品コード ==> 12
該当する商品はありません。
商品コード ==> 85
商品名 : コンパス
単価 : 230
商品コード ==> 99

Z:\clang\kadai08>kadai0813
C言語実習課題8-13 商品検索
商品コード ==> 99
*/