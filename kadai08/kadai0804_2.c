#include <stdio.h>

int main(void)
{
	printf("C言語実習課題8-4 文字列の比較\n");
	
	char str1[256],str2[256];
	int i;
	
	printf("１つの文字列 ==> ");
	scanf("%s",str1);
	printf("２つの文字列 ==> ");
	scanf("%s",str2);
	
	for(i=0;str1[i]==str2[i]&&str1[i];i++){
	}
	
	if(str1[i]==str2[i]){
		printf("%s = %s\n",str1,str2);
	}else if(str1[i]>str2[i]){
		printf("%s > %s\n",str1,str2);
	}else{
		printf("%s < %s\n",str1,str2);
	}
	
	return 0;
}

/*
Z:\clang\kadai08>kadai0804
C言語実習課題8-4 文字列の比較
１つの文字列 ==> abc
２つの文字列 ==> abbc
abc > abbc

Z:\clang\kadai08>kadai0804
C言語実習課題8-4 文字列の比較
１つの文字列 ==> abc
２つの文字列 ==> abd
abc < abd

Z:\clang\kadai08>kadai0804
C言語実習課題8-4 文字列の比較
１つの文字列 ==> abc
２つの文字列 ==> abc
abc = abc

*/