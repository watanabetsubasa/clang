#include <stdio.h>
int str_length(char *p);
void str_cat(char *p1,char *p2);

int main(void)
{
	printf("C言語実習課題8-11 文字列の連結\n");
	
	char str1[256],str2[256];
	int len1,len2;
	
	printf("１つ目の文字列 ==> ");
	scanf("%s",str1);
	printf("２つ目の文字列 ==> ");
	scanf("%s",str2);
	
	len1=str_length(str1);
	len2=str_length(str2);

	if(len1+len2<=20){
		str_cat(str1,str2);
		
		printf("連結後の１つ目の文字列 : %s\n",str1);
	}else{
		printf("合計で20文字を超えたので連結しません。\n");
	}
	
	return 0;
}

int str_length(char *p)
{
	int i;
	
	for(i=0;*p!='\0';p++,i++){
	}
	
	return i;
}

void str_cat(char *p1,char *p2)
{
	for(;*p1!='\0';p1++){
	}
	
	for(;*p2!='\0';p1++,p2++){
		*p1=*p2;
	}
	*p1='\0';
}



/*
Z:\clang\kadai08>kadai0811
C言語実習課題8-11 文字列の連結
１つ目の文字列 ==> abcdefg
２つ目の文字列 ==> hijklmn
連結後の１つ目の文字列 : abcdefghijklmn

Z:\clang\kadai08>kadai0811
C言語実習課題8-11 文字列の連結
１つ目の文字列 ==> abcdefghi
２つ目の文字列 ==> jklmnopqrst
連結後の１つ目の文字列 : abcdefghijklmnopqrst

Z:\clang\kadai08>kadai0811
C言語実習課題8-11 文字列の連結
１つ目の文字列 ==> abcdefghijklmn
２つ目の文字列 ==> opqrstuvwxyz
合計で20文字を超えたので連結しません。
*/
