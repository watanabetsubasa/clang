#include <stdio.h>
#include <string.h>

int main(void)
{
	printf("C言語実習課題8-8 文字列の連結\n");
	
	char str1[256],str2[256];
	int len1,len2;
	
	printf("１つ目の文字列 ==> ");
	scanf("%s",str1);
	printf("２つ目の文字列 ==> ");
	scanf("%s",str2);
	
	len1=strlen(str1);
	len2=strlen(str2);

	
	if(len1+len2<=20){
		strcat(str1,str2);
		
		printf("連結後の１つ目の文字列 : %s\n",str1);
	}else{
		printf("合計で20文字を超えたので連結しません。\n");
	}
	
	return 0;
}

/*
Z:\clang\kadai08>kadai0808
C言語実習課題8-8 文字列の連結
１つ目の文字列 ==> abcdefg
２つ目の文字列 ==> hijklmn
連結後の１つ目の文字列 : abcdefghijklmn

Z:\clang\kadai08>kadai0808
C言語実習課題8-8 文字列の連結
１つ目の文字列 ==> abcdefghi
２つ目の文字列 ==> jklmnopqrst
連結後の１つ目の文字列 : abcdefghijklmnopqrst

Z:\clang\kadai08>kadai0808
C言語実習課題8-8 文字列の連結
１つ目の文字列 ==> abcdefghijklmn
２つ目の文字列 ==> opqrstuvwxyz
合計で20文字を超えたので連結しません。
*/
