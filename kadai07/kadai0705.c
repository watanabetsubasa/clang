#include <stdio.h>
#define N 10
void output(int n,int *p);

int main(void)
{
	printf("C������K�ۑ�7-5 �z��v�f�̌���\n");
	
	int array1[10]={2,4,6,8,10,12,14,16,18,20},array2[10]={3,6,9,12,15,18,21,24,27,30},*pi,*pj,temp;
	
	printf("*** �����O ***\n");
	output(1,array1);
	output(2,array2);
	
	for(pi=array1,pj=array2;pi<array1+N;pi++,pj++){
		temp=*pi;
		*pi=*pj;
		*pj=temp;
	}
	
	printf("*** ������ ***\n");
	output(1,array1);
	output(2,array2);
	
	return 0;
}

void output(int n,int *p)
{
	int *pi;
	printf("array%d = ",n);
	for(pi=p;pi<p+N;pi++){
		printf("%3d",*pi);
	}
	printf("\n");
}

/*
Z:\clang\kadai07>kadai0705
C������K�ۑ�7-5 �z��v�f�̌���
*** �����O ***
array1 =   2  4  6  8 10 12 14 16 18 20
array2 =   3  6  9 12 15 18 21 24 27 30
*** ������ ***
array1 =   3  6  9 12 15 18 21 24 27 30
array2 =   2  4  6  8 10 12 14 16 18 20
*/