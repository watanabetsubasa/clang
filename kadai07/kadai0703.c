#include <stdio.h>

int main(void)
{
	double height,weight,bmi;
	double *pheight=&height,*pweight=&weight,*pbmi=&bmi;
	
	printf("C言語実習課題7-3 BMI\n");
	
	printf("身長(m) ==> ");
	scanf("%lf",pheight);
	printf("体重(kg) ==> ");
	scanf("%lf",pweight);
	
	*pbmi=*pweight/(*pheight * *pheight);
	printf("BMIは%.2fです。\n",*pbmi);
	
	return 0;
}
/*
Z:\clang\kadai07>kadai0703
C言語実習課題7-3 BMI
身長(m) ==> 1.72
体重(kg) ==> 65.8
BMIは22.24です。

Z:\clang\kadai07>kadai0703
C言語実習課題7-3 BMI
身長(m) ==> 1.8
体重(kg) ==> 55.5
BMIは17.13です。
*/