#include <stdio.h>
void search(int *p,int *pmax,int *pmin);

int main(void)
{
	printf("C言語実習課題7-8 最大値と最小値\n");
	int i,array[5],max,min;
	
	for(i=0;i<5;i++){
		printf("整数 ==> ");
		scanf("%d",&array[i]);
	}
	
	search(array,&max,&min);
	
	printf("最大値 = %d\n",max);
	printf("最小値 = %d\n",min);
	
	return 0;
}

void search(int *p,int *pmax,int *pmin)
{
	int *pi;
	*pmax=*p;
	*pmin=*p;
	
	for(pi=p+1;pi<p+5;pi++){
		if(*pmax<*pi){
			*pmax=*pi;
		}
		if(*pmin>*pi){
			*pmin=*pi;
		}
	}
}








/*
Z:\clang\kadai07>kadai0708
C言語実習課題7-8 最大値と最小値
整数 ==> 5
整数 ==> -100
整数 ==> -70
整数 ==> 200
整数 ==> 60
最大値 = 200
最小値 = -100

Z:\clang\kadai07>kadai0708
C言語実習課題7-8 最大値と最小値
整数 ==> -80
整数 ==> -3
整数 ==> 0
整数 ==> 230
整数 ==> 500
最大値 = 500
最小値 = -80

Z:\clang\kadai07>kadai0708
C言語実習課題7-8 最大値と最小値
整数 ==> 70
整数 ==> 60
整数 ==> 50
整数 ==> 40
整数 ==> 30
最大値 = 70
最小値 = 30
*/