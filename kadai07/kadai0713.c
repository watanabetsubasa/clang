#include <stdio.h>
#define ROW 3
#define	COLUMN 5
void splitArray(int (*p)[5]);
void sort(int *p);
void output(int (*p)[5]);

int main(void)
{
	printf("C言語実習課題7-13 行ごとの整列\n");
	int data[][5]={{40,10,30,20,50},{2,4,1,5,3},{300,200,500,100,400}};
	
	
	printf("*** ソート前 ***\n");
	output(data);
	
	splitArray(data);
	
	printf("*** ソート後 ***\n");
	output(data);
	
	return 0;
}

void splitArray(int (*p)[5])
{
	int (*pi)[5];
	for(pi=p;pi<p+ROW;pi++){
		sort(*pi);
	}
}

void sort(int *p)
{
	int *pi,*pj,temp;
	
	for(pi=p;pi<p+COLUMN-1;pi++){
		for(pj=pi+1;pj<p+COLUMN;pj++){
			if(*pi>*pj){
				temp=*pi;
				*pi=*pj;
				*pj=temp;
			}
		}
	}
}

void output(int (*p)[5])
{
	int (*pi)[5],*pj;
	
	for(pi=p;pi<p+ROW;pi++){
		for(pj=*pi;pj<*pi+COLUMN;pj++){
			printf("%5d",*pj);
		}
		printf("\n");
	}
}




/*
Z:\clang\kadai07>kadai0713
C言語実習課題7-13 行ごとの整列
*** ソート前 ***
   40   10   30   20   50
    2    4    1    5    3
  300  200  500  100  400
*** ソート後 ***
   10   20   30   40   50
    1    2    3    4    5
  100  200  300  400  500
*/