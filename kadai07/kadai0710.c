#include <stdio.h>
int input(void);
int search(int in_code,int *p);
void output(int in_code, int *p,int count);

int main(void)
{
	printf("C言語実習課題7-10 在庫数検索\n");
	
	int code[5]={21,68,37,40,85},stock[5]={123,430,333,650,200},in_code,count;
	
	do{
		in_code=input();
		
		if(in_code!=99){
			count=search(in_code,code);
			
			output(in_code,stock,count);
		}
	}while(in_code!=99);
	
	return 0;
}

int input(void)
{
	int in_code;
	do{
		
		printf("商品コード ==> ");
		scanf("%d",&in_code);
		
		if(!(10<=in_code&&in_code<=90||in_code==99)){
			printf("商品コードは、10から90です。\n");
		}
	}while(!(10<=in_code&&in_code<=90||in_code==99));
	
	return in_code;
}

int search(int in_code,int *p){
	int *pi,count=0;
	
	for(pi=p;pi<p+5&&in_code!=*pi;pi++,count++){
	}
	
	if(pi>=p+5){
		count=-1;
	}
	return count;
}

void output(int in_code,int *p,int count)
{
	if(count!=-1){
		printf("商品コード:%dの在庫数は、%dです。\n",in_code,*(p+count));
	}else{
		printf("該当する商品はありません。\n");
	}
}





/*
Z:\clang\kadai07>kadai0710
C言語実習課題7-10 在庫数検索
商品コード ==> 37
商品コード:37の在庫数は、333です。
商品コード ==> 21
商品コード:21の在庫数は、123です。
商品コード ==> 85
商品コード:85の在庫数は、200です。
商品コード ==> 10
該当する商品はありません。
商品コード ==> 40
商品コード:40の在庫数は、650です。
商品コード ==> 68
商品コード:68の在庫数は、430です。
商品コード ==> 99

Z:\clang\kadai07>kadai0710
C言語実習課題7-10 在庫数検索
商品コード ==> 99

Z:\clang\kadai07>kadai0710
C言語実習課題7-10 在庫数検索
商品コード ==> 9
商品コードは、10から90です。
商品コード ==> 50
該当する商品はありません。
商品コード ==> 91
商品コードは、10から90です。
商品コード ==> 37
商品コード:37の在庫数は、333です。
商品コード ==> 99

*/