#include <stdio.h>

int main(void)
{
	printf("C言語実習課題7-1 ポインタ変数\n");
	char a;
	int b;
	float c;
	double d;
	char *pa=&a;
	int *pb=&b;
	float *pc=&c;
	double *pd=&d;
	printf("char型データ ==> ");
	scanf("%c",pa);
	printf("int型データ ==> ");
	scanf("%d",pb);
	printf("float型データ ==> ");
	scanf("%f",pc);
	printf("double型データ ==> ");
	scanf("%lf",pd);
	
	printf("\n変数 アドレス         サイズ               値\n---- ---------------- ------ ----------------\n");
	
	printf(" a   %p %6zd %16c\n",&a,sizeof(a),a);
	printf(" b   %p %6zd %16d\n",&b,sizeof(b),b);
	printf(" c   %p %6zd %16.3f\n",&c,sizeof(c),c);
	printf(" d   %p %6zd %16.3f\n",&d,sizeof(d),d);
	printf(" pa  %p %6zd %p\n",&pa,sizeof(pa),pa);
	printf(" pb  %p %6zd %p\n",&pb,sizeof(pb),pb);
	printf(" pc  %p %6zd %p\n",&pc,sizeof(pc),pc);
	printf(" pd  %p %6zd %p\n",&pd,sizeof(pd),pd);
	return 0;
}
/*
C言語実習課題7-1 ポインタ変数
char型データ ==> k
int型データ ==> 10000
float型データ ==> 123.456
double型データ ==> 1000.123

変数 アドレス         サイズ               値
---- ---------------- ------ ----------------
 a   000000E4AB30F8B0      1                k
 b   000000E4AB30F8B4      4            10000
 c   000000E4AB30F8B8      4          123.456
 d   000000E4AB30F8E0      8         1000.123
 pa  000000E4AB30F8C0      8 000000E4AB30F8B0
 pb  000000E4AB30F8C8      8 000000E4AB30F8B4
 pc  000000E4AB30F8D0      8 000000E4AB30F8B8
 pd  000000E4AB30F8D8      8 000000E4AB30F8E0
*/