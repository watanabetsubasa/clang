#include <stdio.h>
void sort(int **p);
void output(int **p);

int main(void)
{
	printf("C言語実習課題7-12 整列\n");
	
	int seisu1=40,seisu2=20,seisu3=50,seisu4=10,seisu5=30;
	int *p[5];
	p[0]=&seisu1;
	p[1]=&seisu2;
	p[2]=&seisu3;
	p[3]=&seisu4;
	p[4]=&seisu5;
	
	printf("*** ソート前 ***\n");
	output(p);
	
	sort(p);
	
	printf("*** ソート後 ***\n");
	output(p);
	
	return 0;
}

void sort(int **p)
{
	int **pi,**pj,*temp;
	
	for(pi=p;pi<p+4;pi++){
		for(pj=pi+1;pj<p+5;pj++){
			if(**pi>**pj){
				temp=*pi;
				*pi=*pj;
				*pj=temp;
			}
		}
	}
}

void output(int **p)
{
	int **pi;
	
	for(pi=p;pi<p+5;pi++){
		printf("%5d",**pi);
	}
	printf("\n");
}



/*
Z:\clang\kadai07>kadai0712_2
C言語実習課題7-12 整列
*** ソート前 ***
   40   20   50   10   30
*** ソート後 ***
   10   20   30   40   50
*/