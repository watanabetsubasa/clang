#include <stdio.h>

int main(void)
{
	int seisu1,seisu2,kotae;
	int *pseisu1=&seisu1,*pseisu2=&seisu2,*pkotae=&kotae;
	printf("C言語実習課題7-2 ポインタを使った演算\n");
	printf("１つ目の整数を入力してください ==> ");
	scanf("%d",pseisu1);
	printf("２つ目の整数を入力してください ==> ");
	scanf("%d",pseisu2);
	*pkotae=*pseisu1+*pseisu2;
	printf("%d + %d = %d\n",*pseisu1,*pseisu2,*pkotae);
	return 0;
}
/*
Z:\clang\kadai07>kadai0702
C言語実習課題7-2 ポインタを使った演算
１つ目の整数を入力してください ==> 30
２つ目の整数を入力してください ==> 20
30 + 20 = 50
*/