#include <stdio.h>
void output(double *p);
	
int main(void)
{
	printf("C言語実習課題7-6 整列\n");
	double data[5]={160.7,162.8,155.5,168.3,160.1},*pi,*pj,temp;
	
	printf("*** ソート前 ***\n");
	output(data);
	
	for(pi=data;pi<data+4;pi++){
		for(pj=pi+1;pj<data+5;pj++){
			if(*pi<*pj){
				temp=*pi;
				*pi=*pj;
				*pj=temp;
			}
		}
	}
	
	printf("*** ソート後 ***\n");
	output(data);
	
	return 0;
}

void output(double *p)
{
	double *pi;
	
	for(pi=p;pi<p+5;pi++){
		printf("%7.1f",*pi);
	}
	printf("\n");
}
/*
Z:\clang\kadai07>kadai0706
C言語実習課題7-6 整列
*** ソート前 ***
  160.7  162.8  155.5  168.3  160.1
*** ソート後 ***
  168.3  162.8  160.7  160.1  155.5
*/